<?php

//require_once preg_replace('/wp-content(?!.*wp-content).*/','',__DIR__).'wp-config.php";
$con = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

#transact xml start here
#get builder data attribute
$builder_data = get_builder_data($xml->Builder);

#save builder data attribute and return inserted builder id
$builderid = save_builder($con,$builder_data['buildercode'],$builder_data['state'],$builder_data['name'],$builder_data['attachmentsurl'],$builder_data['email']);

#process through division childs to save each division attribute
$division = $xml->Builder->Division;
for($x=0; $x<=(count($division)-1); $x++){
	
	$divisionid = save_division_data($con,$builder_data['buildercode'],$division[$x]->Code,$division[$x]->Name,$division[$x]->DefaultLeadsEmail);
	
	#get option rule
	$optionrule = $division[0]->optionrules->optionrule;
	get_option_rule($con,$optionrule,$divisionid);
	
	#process subdivision data
	$subdivision = $division[$x]->Subdivision;
	for($s=0; $s<=(count($subdivision)-1); $s++){
		$subdiv_array = array(
			'topoimagepath' 		=> $subdivision[$s]->TopoImagePath,
			'code' 					=> $subdivision[$s]->Code,
			'custportallogourl' 	=> $subdivision[$s]->CustPortalLogoURL,
			'name' 					=> $subdivision[$s]->Name,
			'subleadsemail' 		=> $subdivision[$s]->SubLeadsEmail,
			'addmodscatteredlots' 	=> $subdivision[$s]->AddModScatteredLots,
			'exclude' => $subdivision[$s]->Exclude,
			'legalname' => $subdivision[$s]->LegalName,
			'county' => $subdivision[$s]->County,
			'country' => $subdivision[$s]->Country,
			'fax' => $subdivision[$s]->Fax,
			'hoafees' => $subdivision[$s]->HOAFees,
			'estpropertytax' => $subdivision[$s]->EstPropertyTax,
			'maxloanamt' => $subdivision[$s]->MaxLoanAmt,
			'miscellaneousfee' => $subdivision[$s]->MiscellaneousFee,
			'hoaname' => $subdivision[$s]->HOAName,
			'hoaphone' => $subdivision[$s]->HOAPhone,
			'hoatype' => $subdivision[$s]->HOAType,
			'latitude' => $subdivision[$s]->Latitude,
			'longitude' => $subdivision[$s]->Longitude,
			'marketingdescription' => $subdivision[$s]->MarketingDescription
		);
		
		$subdivisionid = save_subdivision_data($con,$division[$x]->Code,$subdiv_array);
		$salesoffice = $subdivision[$s]->SalesOffice;

		save_sales_address_data($con,$subdivision[$s]->Code,$salesoffice);
		save_salesphone_data($con,$subdivision[$s]->Code,$salesoffice);
		
		$phaselist =  $subdivision[$s]->Phase;
		save_phase_data($con,$subdivision[$s]->Code,$phaselist);
		
		$useragent = $subdivision[$s]->User;
		save_user_data($con,$subdivision[$s]->Code,$useragent);
		
		$subdivplan = $subdivision[$s]->SubdivisionPlans;
		save_subdivision_plan_data($con,$subdivision[$s]->Code,$subdivplan);
	}
}

function save_subdivision_plan_data($con,$subdivcode,$subdivplan_array)
{
	$data = $subdivplan_array;
	$SQL = "delete from builder_subdivisionplan where xml_lock = 'N' and subdivision_code = '$subdivcode'";
	mysqli_query($con,$SQL);
	
	if(count($data) > 0){
		
		$plan = $data->Plan;
		for($c=0; $c<=(count($plan)-1); $c++){
			
			$plannumber = $plan[$c]->PlanNumber;
			$planname = $plan[$c]->PlanName;
			$basePrice = $plan[$c]->BasePrice;
			$basesqft = $plan[$c]->BaseSqft;
			$stories = $plan[$c]->Stories;
			$baths = $plan[$c]->Baths;
			$bedrooms = $plan[$c]->Bedrooms;
			$description = $plan[$c]->Description;
			$minlotsize = $plan[$c]->MinLotSize;
			$features = $plan[$c]->Features;
			$miscellaneous = $plan[$c]->Miscellaneous;
			$constructionDays = $plan[$c]->ConstructionDays;
			$masterplanid = $plan[$c]->MasterPlanID;
			
			$SQL = "select * from builder_subdivisionplan where subdivision_code = '$subdivcode' and plannumber = '$plannumber'";
			$result = mysqli_query($con,$SQL);
			
			if (mysqli_num_rows($result) > 0) {
				$row = mysqli_fetch_array($result,MYSQLI_ASSOC);
				$planid = $row['id'];
				$query = "update builder_subdivisionplan set 
						  planname = '$planname',
						  baseprice = '$basePrice',
						  basesqft = '$basesqft',
						  stories = '$stories',
						  baths= '$baths',
						  bedrooms= '$bedrooms',
						  description='$description',
						  minlotsize='$minlotsize',
						  features='$features',
						  miscellaneous='$miscellaneous',
						  contstructiondays='$constructionDays',
						  masterplanid = '$masterplanid'
						  where id = ".$planid;
				mysqli_query($con,$query);
			}else{
				$query = "Insert into builder_subdivisionplan (
							  subdivision_code,
							  plannumber,
							  planname,
							  baseprice,
							  basesqft,
							  stories,
							  baths,
							  bedrooms,
							  description,
							  minlotsize,
							  features,
							  miscellaneous,
							  contstructiondays,
							  masterplanid 
						  ) values(
						  	  '$subdivcode',
							  '$plannumber',
							  '$planname',
							  '$basePrice',
							  '$basesqft',
							  '$stories',
							  '$baths',
							  '$bedrooms',
							  '$description',
							  '$minlotsize',
							  '$features',
							  '$miscellaneous',
							  '$constructionDays',
							  '$masterplanid'
						  )";
				mysqli_query($con,$query);
				
				#get inserted id
				$SQL = "select * from builder_subdivisionplan where subdivision_code = '$subdivcode' and plannumber = '$plannumber'";
				$result = mysqli_query($con,$SQL);
				$row = mysqli_fetch_array($result,MYSQLI_ASSOC);
				
				$plnid = $row['id'];
				
				$planattach = $plan[$c]->Attachment;
				save_plan_attachment_data($con,$plnid,$planattach);
			}
			
		}
		
	}else{
		
	}
}

function save_plan_attachment_data($con,$plnid,$planattach_array)
{
	set_time_limit(0);
	$data = $planattach_array;
	$SQL = "delete from builder_subdivisionattachment where subdivisionplanid = '$plnid'";
	mysqli_query($con,$SQL);
	
	for($c=0; $c<=(count($data)-1); $c++){
		
		$type = $data[$c]->Type;
		$fileName = $data[$c]->FileName;
		$guid = $data[$c]->GUID;
		$origfilename = $data[$c]->OrigFileName;
		$attachmentdescription = addslashes($data[$c]->Description);
		$defaultselection = $data[$c]->DefaultSelection;
		
		$query = "Insert into builder_subdivisionattachment(
				  subdivisionplanid,
				  type,
				  filename,
				  guid,
				  originalfilename,
				  description,
				  defaultselection
				 )values(
				  '$plnid',
				  '$type',
				  '$fileName',
				  '$guid',
				  '$origfilename',
				  '$attachmentdescription',
				  '$defaultselection'
				 )
				 ";
		mysqli_query($con,$query);
		
	}
}

function save_user_data($con,$subdivcode,$user_array)
{
	$data = $user_array;
	
	for($c=0; $c<=(count($data)-1); $c++){
		
		$username = $data[$c]->UserName;
		$SQL = "select * from builder_agent where subdivision_code = '$subdivcode' and username = '$username'";
		$result = mysqli_query($con,$SQL);
	
		if (mysqli_num_rows($result) > 0) {
	
			$row = mysqli_fetch_array($result,MYSQLI_ASSOC);
			$uid = $row['id'];
			$query = "update builder_agent set 
					  firstname = '".$data[$c]->FirstName."',
					  lastname = '".$data[$c]->LastName."',
					  title = '".$data[$c]->Title."',
					  jobposition = '".addslashes($data[$c]->JobPosition)."',
					  address1= '".addslashes($data[$c]->Address1)."',
					  address2 = '".addslashes($data[$c]->Address2)."',
					  city = '".$data[$c]->City."',
					  zip = '".$data[$c]->Zip."',
					  workphone = '".$data[$c]->WorkPhone."',
					  email = '".$data[$c]->Email."'  
					  where id = ".$uid;
			mysqli_query($con,$query);
			
		}else{
			
			$query = "Insert into builder_agent (
					  subdivision_code,
					  firstname,
					  lastname,
					  title,
					  jobposition,
					  address1,
					  address2,
					  city,
					  zip,
					  workphone,
					  email,
					  username
					 ) values (
					  '".$subdivcode."',
					  '".$data[$c]->FirstName."',
					  '".$data[$c]->LastName."',
					  '".$data[$c]->Title."',
					  '".addslashes($data[$c]->JobPosition)."',
					  '".addslashes($data[$c]->Address1)."',
					  '".addslashes($data[$c]->Address2)."',
					  '".$data[$c]->City."',
					  '".$data[$c]->Zip."',
					  '".$data[$c]->WorkPhone."',
					  '".$data[$c]->Email."',
					  '".$data[$c]->UserName."')";
			mysqli_query($con,$query);
		}
	}
	
	return true;
}

function save_phase_data($con,$subdivcode,$phase_array)
{
	$data = $phase_array;
	for($c=0; $c<=(count($data)-1); $c++){
		
		$phasename = $data[$c]->Name;
		$topoimagepath = $data[$c]->TopoImagePath;
		$SQL = "select * from builder_subdivisionphase where subdivision_code = '$subdivcode' and name = '$phasename'";
		$result = mysqli_query($con,$SQL);
	
		if (mysqli_num_rows($result) > 0) {
	
			$row = mysqli_fetch_array($result,MYSQLI_ASSOC);
			$phid = $row['id'];
			$query = "update builder_subdivisionphase set name = '$phasename',topoimagepath = '$topoimagepath'  where id = ".$phid;
			mysqli_query($con,$query);
			
		}else{
			
			$query = "insert into builder_subdivisionphase (subdivision_code,name,topoimagepath) values ('$subdivcode','$phasename','$topoimagepath')";
			mysqli_query($con,$query);
		}
	}
	
	return true;
}
function save_salesphone_data($con,$subdivcode,$salesoffice_array)
{
	$data = $salesoffice_array;

	$SQL = "select * from builder_salesoffice where subdivision_code = '$subdivcode'";
	$result = mysqli_query($con,$SQL);

	if (mysqli_num_rows($result) > 0) {

		$row = mysqli_fetch_array($result,MYSQLI_ASSOC);
		$salesid = $row['id'];
		$query = "update builder_salesoffice set phoneareacode= '".$data->Phone->AreaCode."',phoneprefix = '".$data->Phone->Prefix."',phonesuffix = '".$data->Phone->Suffix."'  where subdivision_code = '".$subdivcode."'";
		mysqli_query($con,$query);
		
	}else{
		
		$query = "Insert into builder_salesoffice (subdivision_code,phoneareacode,phoneprefix,phonesuffix) values('$subdivcode','".$data->Phone->AreaCode."','".$data->Phone->Prefix."','".$data->Phone->Suffix."')";
		mysqli_query($con,$query);
	}
	
	return true;
}

function save_sales_address_data($con,$subdivcode,$salesoffice_array)
{
	$data = $salesoffice_array;
	$SQL = "select * from builder_salesoffice where subdivision_code = '$subdivcode'";
	$result = mysqli_query($con,$SQL);

	if (mysqli_num_rows($result) > 0) {

		$row = mysqli_fetch_array($result,MYSQLI_ASSOC);
		$salesid = $row['id'];
		$query = "update builder_salesoffice set city= '".$data->Address->City."',state = '".$data->Address->State."',zip = '".$data->Address->ZIP."'  where subdivisioncode = '".$subdivcode."'";
		mysqli_query($con,$query);
		
	}else{
		
		$query = "Insert into builder_salesoffice (subdivision_code,city,state,zip) values('$subdivcode','".$data->Address->City."','".$data->Address->State."','".$data->Address->ZIP."')";
		mysqli_query($con,$query);
	}
	
	return true;
}

function save_subdivision_data($con,$divisioncode,$subdiv_array)
{
	$subdivcode = $subdiv_array['code'];
	$SQL = "select * from builder_subdivision where code = '$subdivcode' and division_code = '$divisioncode'";
	
	$result = mysqli_query($con,$SQL);
	
	if (mysqli_num_rows($result) > 0) {

		$row = mysqli_fetch_array($result,MYSQLI_ASSOC);
		$sid = $row['id'];
		
		if($row['xml_lock'] == 'Y'){
			return $sid;
		}else{
			$query = "update builder_subdivision set 
					  custportallogourl = '".$subdiv_array['custportallogourl']."',
					  name= '".$subdiv_array['name']."',
					  subleadsemail = '".$subdiv_array['subleadsemail']."',
					  addmodscatteredlots = ".$subdiv_array['addmodscatteredlots'].",
					  exclude = '".$subdiv_array['exclude']."',
					  legalname = '".$subdiv_array['legalname']."',
					  county = '".$subdiv_array['county']."',
					  country = '".$subdiv_array['country']."',
					  fax = '".$subdiv_array['fax']."',
					  hoafees = '".$subdiv_array['hoafees']."',
					  estpropertytax = '".$subdiv_array['estpropertytax']."',
					  maxloanamount = '".$subdiv_array['maxloanamt']."',
					  miscellaneousfee = '".$subdiv_array['miscellaneousfee']."',
					  hoaname='".$subdiv_array['hoaname']."',
					  hoatype = '".$subdiv_array['hoatype']."',
					  latitude = '".$subdiv_array['latitude']."',
					  longitude = '".$subdiv_array['longitude']."',
					  marketingdescription = '".addslashes($subdiv_array['marketingdescription'])."',
					  topoimagepath = '".addslashes($subdiv_array['topoimagepath'])."'
					  where id = ".$sid;

			mysqli_query($con,$query);
		}
		
	}else{
		
		$query = "Insert into builder_subdivision (
					division_code,
					code,
					custportallogourl,
					name,
					subleadsemail,
			 		addmodscatteredlots,
			 		exclude,
			 		legalname,
			 		county,
			 		country,
			 		fax,
			 		hoafees,
			 		estpropertytax,
			 		maxloanamount,
			 		miscellaneousfee,
			 		hoaname,
			 		hoatype,
			 		latitude,
			 		longitude,
			 		marketingdescription,
			 		topoimagepath
			 	 ) values(
			 	 	'$divisioncode',
			 	 	'$subdivcode',
			 	 	'".$subdiv_array['custportallogourl']."',
			 	 	'".$subdiv_array['name']."',
			 	 	'".$subdiv_array['subleadsemail']."',
			 	 	".$subdiv_array['addmodscatteredlots'].",
			 	 	'".$subdiv_array['exclude']."',
			 	 	'".$subdiv_array['legalname']."',
			 	 	'".$subdiv_array['county']."',
			 	 	'".$subdiv_array['country']."',
			 	 	'".$subdiv_array['fax']."',
			 	 	'".$subdiv_array['hoafees']."',
			 	 	'".$subdiv_array['estpropertytax']."',
			 	 	'".$subdiv_array['maxloanamt']."',
			 	 	'".$subdiv_array['miscellaneousfee']."',
			 	 	'".$subdiv_array['hoaname']."',
			 	 	'".$subdiv_array['hoatype']."',
			 	 	'".$subdiv_array['latitude']."',
			 	 	'".$subdiv_array['longitude']."',
			 	 	'".addslashes($subdiv_array['marketingdescription'])."',
			 	 	'".addslashes($subdiv_array['topoimagepath'])."'
			 	   )";
		mysqli_query($con,$query);
		
		#get inserted id
		$SQL = "select * from builder_subdivision where code = '$subdivcode' and division_code = '$divisioncode'";
		$result = mysqli_query($con,$SQL);
		$row = mysqli_fetch_array($result,MYSQLI_ASSOC);
		
		$sid = $row['id'];
	}
	
	return $sid;
}

function get_builder_data($builder)
{
	$data['buildercode'] = $builder->BuilderCode;
	$data['state'] = $builder->State;
	$data['name'] = $builder->Name;
	$data['attachmentsurl'] = $builder->AttachmentsURL;
	$data['email'] = $builder->Email;
	
	return $data;
	
}

function save_builder($con, $buildercode, $state, $name, $attachmenturl, $email)
{
	$SQL = "select * from builder_master where builder_code = '$buildercode'";
	$result = mysqli_query($con,$SQL);

	if (mysqli_num_rows($result) > 0) {

		$row = mysqli_fetch_array($result,MYSQLI_ASSOC);
		$builderid = $row['id'];
		$query = "update builder_master set builder_code = '$buildercode', state = '$state',name = '$name',email= '$email' where id = ".$builderid;
		mysqli_query($con,$query);
		
	}else{
		
		$query = "Insert into builder_master (builder_code,state,name,attachmenturl,email) values('$buildercode','$state','$name','$attachmenturl','$email')";
		mysqli_query($con,$query);
		
		#get inserted id
		$SQL = "select id from builder_master where builder_code = '$buildercode'";
		$result = mysqli_query($con,$SQL);
		$row = mysqli_fetch_array($result,MYSQLI_ASSOC);
		
		$builderid = $row['id'];
	}
	
	return $builderid;

}


function save_division_data($con, $buildercode, $code, $name, $defaultemail)
{
		$SQL = "select * from builder_division where code = '$code'";
		$result = mysqli_query($con,$SQL);
		
		if (mysqli_num_rows($result) > 0) {
			
			$row = mysqli_fetch_array($result,MYSQLI_ASSOC);
			$divisionid = $row['id'];
			$query = "update builder_division set builder_code= '$buildercode',code = '$code',name = '$name',defaultleadsemail= '$defaultemail' where id = ".$divisionid;
			mysqli_query($con,$query);
			
		}else{
			
			$query = "Insert into builder_division (builder_code,code,name,defaultleadsemail) values('$buildercode','$code','$name','$defaultemail')";
			mysqli_query($con,$query);
			
			#get inserted id
			$SQL = "select * from builder_division where code = '$code'";
			$result = mysqli_query($con,$SQL);
			$row = mysqli_fetch_array($result,MYSQLI_ASSOC);
			
			$divisionid = $row['id'];
			
		}	
		
		return $divisionid;
}

function get_option_rule($con,$optionrule,$divisionid)
{
	
	for($x=0; $x<=(count($optionrule)-1); $x++){
		
		$optionattrib = $optionrule[$x]->attributes();
		$ruletype = $optionattrib['ruletype'];
		$rulename = $optionattrib['name'];
		
		$option = $optionrule[$x]->option;
		for($i=0; $i<=(count($option) - 1); $i++){
			
			
			$masteroption1id = "";
			$mastertmpid = $option[$i]->attributes();
			$masteroptionid1 = $mastertmpid['masteroptionid'];
			
			$tmptargetlist = $option[$i]->TargetMasterOptionID;
			if(count($tmptargetlist) > 1){
				
				$masteroptionid2 = "";
				for($z=0; $z<=(count($tmptargetlist)-1); $z++){
					$tmpoid2 = $tmptargetlist[$z]->attributes();
					$masteroptionid2 .= $tmpoid2['masteroptionid'];
					if($z < (count($tmptargetlist)-1)){
						$masteroptionid2 .= ",";		
					}
				}
				
			}else{
				
				$masteroptionid2 = "";
				$tmpoid2 = $tmptargetlist->attributes();
				$masteroptionid2 .= $tmpoid2['masteroptionid'];
				
			}
			
			#---
			$SQL = "select id from builder_optionrule where divisionid = '$divisionid' and option_master = '$masteroptionid1' and rulename = '$rulename'";
			$result = mysqli_query($con,$SQL);
			
			if(mysqli_num_rows($result) > 0) {
				
				$row = mysqli_fetch_array($result,MYSQLI_ASSOC);
				$optid = $row['id'];
				$query = "update builder_optionrule set option_required = '$masteroptionid2', option_type  = '$ruletype' where id = ".$optid;
				mysqli_query($con,$query);
			
			}else{
				
				$query = "insert into builder_optionrule (option_master,option_required,option_type,divisionid,rulename) values ('$masteroptionid1','$masteroptionid2','$ruletype',$divisionid,'$rulename')";
				mysqli_query($con,$query);
			
			}
		}
		
	}
}

?>