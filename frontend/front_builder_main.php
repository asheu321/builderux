
<?php include 'front_header.php'; ?>

<?php

require_once get_home_path().'wp-load.php';
global $wpdb; 

$result = $wpdb->get_results("select * from builder_slides");

?>
<div style="padding: 20px;">
<div class="alert alert-success" role="alert">
<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> Manage Slide data, these setting will setup the slide for builder plugin. You can put it in the post or page or in the php code format:
<br />
Post/Page: "[get_slider]"
<br />
PHP Format: "get_slider_func()"
<br />
</div>
<div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">Slides Settings</h3>
  </div>
  <div class="panel-body">
	<table class="table table-striped" id="builder-slides"> 
		<thead> 
			<tr> 
				<th>#</th> 
				<th style="width: 20%;">Title</th> 
				<th style="width: 50%;">Content</th> 
				<th style="width: 30%;">Path / Link / Url</th> 
			</tr> 
		</thead> 
		<tbody> 
			<?php
				$cnt = 1;
				foreach($result as $key => $obj){ ?>
					<tr> 
						<th scope="row"><?php echo $cnt; ?></th> 
						<td><a href="javascript: void(0)" id="slide_title" class="slide_title" data-type="text" data-pk="<?php echo $obj->slide_id; ?>" data-url="<?php echo plugins_url('frontend/trans/trans_slides.php',dirname(__FILE__)); ?>?funct=updatecel" data-title="Enter Title"><?php echo $obj->slide_title; ?></a></td> 
						<td><a href="javascript: void(0)" id="slide_content" class="slide_content" data-type="textarea" data-pk="<?php echo $obj->slide_id; ?>" data-url="<?php echo plugins_url('frontend/trans/trans_slides.php',dirname(__FILE__)); ?>?funct=updatecel" data-title="Enter Content"><?php echo $obj->slide_content; ?></a></td> 
						<td><a href="javascript: void(0)" id="image_path" class="image_path" data-type="textarea" data-pk="<?php echo $obj->slide_id; ?>" data-url="<?php echo plugins_url('frontend/trans/trans_slides.php',dirname(__FILE__)); ?>?funct=updatecel" data-title="Enter Path-Link-Url"><?php echo $obj->image_path; ?></a></td> 						
					</tr> 
					
					<?php $cnt++;
				}
			?>			
		</tbody> 
	</table>
	</div>
</div>
</div>
<?php include 'front_footer.php'; ?>
<script>
	$(document).ready(function() {
	  $.fn.editable.defaults.mode = 'popup';
	  
	  //$('#builder-slides').on( 'draw.dt', function () {
	    $('.slide_title').editable();
	   	$('.slide_content').editable();
	   	$('.image_path').editable();
	  //});
  });
</script>