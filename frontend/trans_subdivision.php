<?php
include "../../../../../wp-config.php";
global $wpdb; 

$funct = $_GET['funct'];
$page = $_GET['page'];

if($funct == 'fetch_subdivision' && $page == 'choose_your_home'){
	
	$data = array();
	$result = $wpdb->get_results("select * from builder_subdivision");
	
	$html = '<table class="table">';
	$subcount = 0;
	$open_close = 0;
	foreach($result as $key => $obj){
		
		if($subcount == 0){$html .= '<tr><td>'; $open_close = 1;}
		if(strlen($obj->custportallogourl) > 0){$img = $obj->custportallogourl;}else{$img = plugins_url('../assets/images/no-image.png',dirname(__FILE__));}
		$html .= '<div style="width: 25%; float: left; padding: 5px;">					 
					   <img src="'.$img.'" style="width: 100%; height: 200px;">
					     <h3>'.$obj->name.'</h3>
					     <p>'.$obj->marketingdescription.'</p>
					     <p><input type="button" onClick="chooseNext(1,'.$obj->id.')" value="Select Subdivision" /></p>
				  </div>';
		$subcount++;
		if($subcount == 4){$html .= '</td></tr>'; $subcount=0; $open_close = 0;}
	}
	if($open_close == 1){$html .= '</td></tr>';}
	$html .= '</table>';
	
	die(json_encode(array('success' => true, 'subdivision' => $html)));
}

#== function for where we build (where_we_build_view.php)

if($funct == 'fetch_subdivision' && $page == 'where_we_build'){
	
	$data = array();
	
	$sql = "SELECT 
			a.*,
			(SELECT MIN(baseprice) FROM builder_subdivisionplan WHERE subdivision_code = a.code) AS price_min,
			(SELECT MAX(baseprice) FROM builder_subdivisionplan WHERE subdivision_code = a.code) AS price_max,
			(SELECT MIN(basesqft) FROM builder_subdivisionplan WHERE subdivision_code = a.code) AS sqft_min,
			(SELECT MAX(basesqft) FROM builder_subdivisionplan WHERE subdivision_code = a.code) AS sqft_max,
			b.subdivision_code,
			b.city,
			b.state FROM builder_subdivision a 
			LEFT JOIN builder_salesoffice b 
			ON b.subdivision_code = a.code";
			
	$result = $result = $wpdb->get_results($sql);
	
	$html = '<table class="table" style="width: 100%" class="wwb_table">';
	$subcount = 0;
	$open_close = 0;
	foreach($result as $key => $obj){
		if($subcount == 0){$html .= '<tr><td>'; $open_close = 1;}
		if(strlen($obj->custportallogourl) > 0){$img = $obj->custportallogourl;}else{$img = plugins_url('../assets/images/no-image.png',dirname(__FILE__));}
		$html .= '<div style="width: 25%; float: left; padding: 5px;"  class="wwb_div">
					   <img src="'.$img.'" style="width: 300px; height: 200px;" class="wwb_img">
					     <h4 class="wwb_h4">'.$obj->name.'</h4>
					     <p><strong>Location:</strong> '.$obj->city.', '.$obj->state.'<br />
					     <strong>Price Range :</strong> $'.$obj->price_min.' - $'.$obj->price_max.'<br />
					     <strong>SQ FT Range :</strong> '.$obj->sqft_min.' - '.$obj->sqft_max.'</p>

							  <ul style="list-style: none; background-color: #ccc; padding: 5px;"  class="wwb_ul">
							    <li><a href="javascript: void(0)">Inquiry About Community</a></li>
							    <li><a href="'.get_site_url().'/builderux-subdiv-details/?subcode='.$obj->code.'">View Details</a></li>
							    <li role="separator" class="divider"></li>
							    <li><a href="'.get_site_url().'/builderux-floor-plan/?subcode='.$obj->code.'">Floor Plans</a></li>
							    <li><a href="'.get_site_url().'/builderux-movein-ready-house/?subcode='.$obj->code.'">Available Move In Ready</a></li>						    
							    <li><a href="'.get_site_url().'/builderux-model-house/?subcode='.$obj->code.'">Available Model Homes</a></li>
							  </ul>

					     <p style="margin-top: 10px;" class="wwb_mkdescription">'.$obj->marketingdescription.'</p>			    

				  </div>';
		$subcount++;
		if($subcount == 4){$html .= '</td></tr>'; $subcount=0; $open_close = 0;}
	}
	if($open_close == 1){$html .= '</td></tr>';}
	$html .= '</table>';
	
	die(json_encode(array('success' => true, 'subdivision' => $html)));
}

if($funct == 'fetch_subdivsion_detail')
{
	$code = $_GET['code'];
	$html = '';
	
	$sql = "SELECT 
			a.*,
			(SELECT MIN(baseprice) FROM builder_subdivisionplan WHERE subdivision_code = a.code) AS price_min,
			(SELECT MAX(baseprice) FROM builder_subdivisionplan WHERE subdivision_code = a.code) AS price_max,
			(SELECT MIN(basesqft) FROM builder_subdivisionplan WHERE subdivision_code = a.code) AS sqft_min,
			(SELECT MAX(basesqft) FROM builder_subdivisionplan WHERE subdivision_code = a.code) AS sqft_max,
			b.subdivision_code,
			b.city,
			b.state FROM builder_subdivision a 
			LEFT JOIN builder_salesoffice b 
			ON b.subdivision_code = a.code
			where a.code = '$code'";
			
	$result = $result = $wpdb->get_results($sql);	
	
	$info = $result[0];
	$html .= ' <table style="width: 100%; 	table-layout: auto !important;">
					<tr><td colspan="2" style="width: 100%;"><h4 style="margin-bottom: 10px; margin-top: 10px; text-align: center;">'.$info->name.'</h4></td></tr>
					<tr>		
					<td style="background: #f1f1f1; width: 30%; vertical-align: top;"> 	
					  <ul style="list-style: none; background-color: #ccc; padding: 5px;">
					  		<li><a href="'.get_site_url().'/builderux-where-we-build">Back to Community</a></li>						    
							<li role="separator" class="divider"></li>
							<li><a href="'.get_site_url().'/builderux-floor-plan/?subcode='.$info->code.'">Floor Plans</a></li>
							<li><a href="'.get_site_url().'/builderux-movein-ready-house/?subcode='.$info->code.'">Available Move In Ready</a></li>						    
							<li><a href="'.get_site_url().'/builderux-model-house/?subcode='.$info->code.'">Available Model Homes</a></li>
					  </ul>
					</td>
					<td style="width: 70%;">
				  		<p style="text-align: center; margin-top: 10px;">
				  			<img src="'.$info->custportallogourl.'" style="width: 740px;">
				  		</p>
				  		<table style="width: 100%;">
				  			<tr>
				  				<td>'.$info->city.', '.$info->state.'</td>
				  				<td>$'.$info->price_min.' - $'.$info->price_max.'</td>
				  				<td>'.$info->sqft_min.' - '.$info->sqft_max.'</td>
				  			</tr>
				  		</table>
					  	<h4>Community Description</h4>
					  	<p>'.$info->marketingdescription.'</p>
					  	<h4>Topo Map</h4>
					  	<p>
					  		<object width="700" data="http://www.salessimplicity.net/livedemo/topo/default.aspx?SubdivisionNum='.$code.'&Zoom=0.85&ShowLotStatus=yes&ShowLot=yes&ShowPhasePlan=yes&ShowLotSize=yes&ShowTotalPrice=yes&ShowStage=yes&ShowPremium=yes&ShowGarageOrient=no&Certificate=11d18320-0c2e-43a2-a22e-cbb7c3ec780a" type="text/html" class="span12" height="620"></object>
					  	</p>
					  	<h4>Map Directions</h4>
					  	<div class="map" id="map-canvas">
					</td>
					</tr>
				</table>';
				
	die(json_encode(array("success" => true, "info" => $html, "lat" => $info->latitude, "long" => $info->longitude, "title" => $info->name)));
	
}

function getaddress($lat,$lng)
{
	$url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($lat).','.trim($lng).'&sensor=false';

	$json = file_get_contents($url);
	$data = json_decode($json);
	echo "<pre>";
	print_r($data); die();
	$status = $data->status;
	if($status=="OK"){
		return $data->results[0]->formatted_address;
	}else{
		return false;
	}
}
?>