<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&amp;language=en"></script>
<style>
#map-canvas {
     height: 450px;
     margin: 0px;
     padding: 0px;
     margin-bottom: 100px;
}
	
#map-canvas img 
{
    max-width: none;
}
</style>
<div class="wwb_div_caption"><h1 class="wwb_h1_title">Where We Build</h1></div>
<div class="map" id="map-canvas"></div>

<script src="<?php echo plugins_url('assets/js/jquery-1.11.3.min.js',dirname(__FILE__)); ?>"></script> 
<script>

	var geocoder;
	var map;
	var infoWindow = null;
	var beaches = [];
	var markersArray = new Array();
	var siteurl = '';
	
	function getMapInfo(){
		$('#step1').css("background-color", "#cccccc");
		
		$.post(
			"<?php echo plugins_url('frontend/trans/trans_map.php',dirname(__FILE__)); ?>?funct=fetch_mainmap&page=choose_your_home&orderby=name",
		    function(data){
			    siteurl = data.siteurl;
		        initBeaches(data.mapinfo);
		    },
		    'json'
		);	
	}
	
	function initialize(flat,flong,beach) {
		  geocoder = new google.maps.Geocoder();
		  var latlng = new google.maps.LatLng(flat,flong);
		  var mapOptions = {
		    zoom: 8,
		    center: latlng
		  }
		  
		  infoWindow = new google.maps.InfoWindow({content:"holding..."});
		  
		  map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
		  google.maps.event.trigger(map, 'resize');
  		  setMarkers(map,beach);
	}
	
	function initBeaches(infos){
		
		for(var x=0; x<=(infos.length - 1); x++){
			var temp = infos[x];
			beaches.push([temp.name,temp.latitude,temp.longitude,temp.linkid,temp.code,temp.name,temp.logourl,temp.price_range,temp.marketingdescription]);
		}
		
		initialize(infos[0].latitude,infos[0].longitude,beaches);
		
	}
	
	function setMarkers(map, locations) {
		  
		  var pinIcon = new google.maps.MarkerImage(
			    "<?php echo plugins_url('assets/images/map.png',dirname(__FILE__)); ?>",
			    null, /* size is determined at runtime */
			    null, /* origin is 0,0 */
			    null, /* anchor is bottom center of the scaled image */
			    new google.maps.Size(68, 68)
			);  
		 
		 
		  for (var i = 0; i < locations.length; i++) {
		  	
		  	var beach = locations[i];
		  	
		  	  var mycontent = "<div style='width: 600px;'>"+
			    				"<div style='width: 200px; float: left !important;'><img style='width: 190px;' src='"+beach[6]+"' alt='"+beach[0]+"' /></div>"+
			    		 		"<div class='map_desc' style='width: 384px; float: right !important;'>"+
			    		 			"<h6 style='margin:0px !important'>"+beach[0]+"</h6>"+
			    		 			"<hr  style='margin:0px !important'><p class='pop-desc'>"+
			    		 			beach[8]+
			    		 			"</p>"+
			    		 			"Price Range: $"+beach[7]+
			    		 			"<br>"+
			    		 			"<button class='btn btn-primary ' onclick=linkTo('"+beach[4]+"')>View Details</button>"+
			    		 			"</div>"+
			    		 "</div>";
		  	
		  	
		  	var  myLatLng = new google.maps.LatLng(beach[1], beach[2]);
		  	addMarker(myLatLng,beach[0],mycontent,pinIcon);
			
		  }
	}
	
	function linkTo(urlx){
		window.location.href = siteurl + "/builderux-subdiv-details/?subcode=" + urlx;	
	}
	
	function addMarker(m_position,m_title,m_infowindow,icon) {
		var mark;
		if (markersArray.length!=0) 
		{
			duplicate = false;
			var markcopy;
			var markersCopy = [];
			while(markcopy=markersArray.pop())
			{
				if((markcopy.position.lat()==m_position.lat())&&(markcopy.position.lng()==m_position.lng())) duplicate = true;
				markersCopy.push(markcopy);
			}
			markersArray = markersCopy;
			if(duplicate==false)
			{
				marker = new google.maps.Marker({
				  	position: m_position,
				  	map: map,
				  	icon:icon,
					title: m_title
				});
				markersArray.push(marker);
				mark = markersArray.pop();
				google.maps.event.addListener(mark, 'click', function() {
					infoWindow.open(map,mark);
					var stringContent = m_infowindow;
	 				infoWindow.setContent(stringContent);
	 				infoWindow.open(map, this);
					
				});
				markersArray.push(mark);
			}
		}
		else
		{
			marker = new google.maps.Marker({
			  	position: m_position,
			  	map: map,
			  	icon:icon,
				title: m_title
			});
			markersArray.push(marker);
			mark = markersArray.pop();
			google.maps.event.addListener(mark, 'click', function() {
				infoWindow.open(map,mark);
				var stringContent = m_infowindow;
	 			infoWindow.setContent(stringContent);
	 			infoWindow.open(map, this);
			});
			markersArray.push(mark);
		}
	}

	
	getMapInfo();
</script>