<div style="width: 100%; overflow: auto;">
	<div id="floorplan-data"><center><h4>Loading Floorplan...</h4></center></div>
</div>

<script src="<?php echo plugins_url('assets/js/jquery-1.11.3.min.js',dirname(__FILE__)); ?>"></script> 

<script>
	function loadSubdivisionfloorplan(){
		$.post(
			"<?php echo plugins_url('frontend/trans/trans_subdivisionplan.php',dirname(__FILE__)); ?>?funct=fetch_floorplan&page=floorplan",
			{subcode: '<?php echo $_GET['subcode']; ?>'},
		    function(data){
		        $('#floorplan-data').html(data.plan);
		    },
		    'json'
		);	
	}
	
	loadSubdivisionfloorplan();
</script>