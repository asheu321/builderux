<div style="width: 100%; overflow: auto;">
<table style="width: 100%; 	table-layout: auto !important;" class="mhd_table">
		<tr>
			<td style="width: 50%; border-right: solid 0px;" class="mhd_td"><h4 class="mhd_h4" style="text-align: left; margin-bottom: 10px; margin-top: 10px;" id="detail-address"></h4></td>
			<td style="width: 50%; border-left: solid 0px; text-align: right;" class="mhd_td"><!-- <input type="button" value="Inquire About this Property" onclick="inquireNow()"> --></td>
		</tr>
		<tr><td style="width: 70%;" class="mhd_td">
			<p style="margin-top: 10px; border-bottom: solid 1px #ccc; padding-bottom: 10px;" id="restriction"></p>
	  		<p style="font-weight: bold; margin-bottom: 10px; margin-top: 10px;" class="pln_heading">Lot Images</p>
		  	<div id="lot-images" style="width: 100%;"></div>

		  	<p style="font-weight: bold; margin-bottom: 10px; margin-top: 10px;" class="pln_heading">Plan Images</p>
		  	<div id="fplan-images" style="width: 100%;" class="mhd_pn1"></div>
		  	<div id="mplan-images" style="width: 100%;" class="mhd_pn2"></div>
		</td><td style="width: 30%; vertical-align: top; padding: 0px;" class="mhd_tddetail"> 	
		  <div id="specs" class="mhd_specs">
		  <p style="font-weight: bold; margin-bottom: 10px; margin-top: 10px;" class="mhd_opt">Details</p>
		  <div class="panel-body" id="information"></div>
		  
		  <div class="panel-body" id="lot-option"></div></div>
		</td></tr>
	</table>
</div>

<script src="<?php echo plugins_url('assets/js/jquery-1.11.3.min.js',dirname(__FILE__)); ?>"></script> 

<script>
	function loadSubdivisionmoveinready(){

		$.post(
			"<?php echo plugins_url('frontend/trans/trans_phaselot.php',dirname(__FILE__)); ?>?funct=fetch_moveinreadyhouse_detail&page=moveinreadyhouse&mpid=<?php echo $_GET['mpid']; ?>&linkid=<?php echo $_GET['linkid']; ?>",
			{subcode: '<?php echo $_GET['subcode']; ?>'},
		    function(data){
		        $('#detail-address').html(data.address);
		        $('#lot-images').html(data.lotimages);
		        $('#fplan-images').html(data.fplanimages);
		        $('#mplan-images').html(data.mplanimages);
		        $('#information').html(data.info);
		        $('#lot-option').html(data.opt);
		        $('#restriction').html(data.restriction);
		    },
		    'json'
		);	
	}
	
	loadSubdivisionmoveinready();
</script>