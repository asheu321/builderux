<div style="width: 100%; overflow: auto;" class="rq_div">
	<table class="rq_table">
		<tr><td style="padding: 20px; background: #f1f1f1;" class="rq_td">
			<label for="name">First Name *</label>
			<input id="fname" type="text" name="fname" placeholder="*">
			<br /><br />
			<label for="name">Last Name *</label>
			<input id="lname" type="text" name="lname" placeholder="*">
			<br /><br />
			<label for="name">Phone *</label>
			<input id="phone" type="text" name="phone" placeholder="*">
			<br /><br />
			<label for="name">Email Address *</label>
			<input id="email" type="text" name="email" placeholder="*">
			<br /><br />
			<label for="email">Traffic Source</label>
			<select name="traffic_source" id="traffic_source" style="width: 47%; padding: 10px; border: solid 1px #ccc; margin: auto;">
				<option value="Apartment Guide">Apartment Guide</option>
				<option value="Billboard">Billboard</option>
				<option value="Billboards">Billboards</option>
				<option value="Chicago Tribune">Chicago Tribune</option>
				<option value="Direct Mail">Direct Mail</option>
				<option value="Home Rental Guide">Home Rental Guide</option>
				<option value="Internet">Internet</option>
				<option value="Internet Lead">Internet Lead</option>
				<option value="Magazine">Magazine</option>
				<option value="Military Flyer">Military Flyer</option>
				<option value="Newspaper">Newspaper</option>
				<option value="Open House">Open House</option>
				<option value="Other">Other</option>
				<option value="Radio">Radio</option>
				<option value="Realtor">Realtor</option>
				<option value="Referral">Referral</option>
				<option value="Referral - Friend / Family">Referral - Friend / Family</option>
				<option value="Referral - Homeowner">Referral - Homeowner</option>
				<option value="Republic">Republic</option>
				<option value="Sponsorship / Special Event">Sponsorship / Special Event</option>
				<option value="Sponsorship/Special Event">Sponsorship/Special Event</option>
				<option value="Television">Television</option>
				<option value="Tribune">Tribune</option>
			</select>
			<br />
			<label for="email">Occupation</label>
			<select name="occupation_source" id="occupation_source" style="width: 47%; padding: 10px; border: solid 1px #ccc; margin: auto;">
				<option value="Athlete">Athlete</option>
				<option value="Attorney">Attorney</option>
				<option value="Banker">Banker</option>
				<option value="Construction">Construction</option>
				<option value="Doctor">Doctor</option>
				<option value="Electrician">Electrician</option>
				<option value="Firefighter">Firefighter</option>
				<option value="Government Employee">Government Employee</option>
				<option value="Handyman">Handyman</option>
				<option value="Instructor">Instructor</option>
				<option value="Janitor">Janitor</option>
				<option value="Karate master">Karate master</option>
				<option value="Law enforcement agent">Law enforcement agent</option>
				<option value="Manager">Manager</option>
				<option value="Notary">Notary</option>
				<option value="Other">Other</option>
				<option value="Project Manager">Project Manager</option>
				<option value="Quilter">Quilter</option>
				<option value="Registered Nurse">Registered Nurse</option>
				<option value="Salesmen">Salesmen</option>
				<option value="Teacher">Teacher</option>
				<option value="Underwriter">Underwriter</option>
				<option value="Video Game Developer">Video Game Developer</option>
				<option value="Waiting Staff">Waiting Staff</option>
				<option value="Xylophonist">Xylophonist</option>
				<option value="Yodeler">Yodeler</option>
				<option value="Zoologist">Zoologist</option>
			</select>
			<br />
			<label for="email">Family Size</label>
			<select name="family_size_source" id="family_size_source" style="width: 47%; padding: 10px; border: solid 1px #ccc; margin: auto;">
				<option value="0-2">0-2</option>
				<option value="3 - 5">3 - 5</option>
				<option value="6 - 10">6 - 10</option>
				<option value="11 - 15">11 - 15</option>
				<option value="15 - 18">15 - 18</option>
			</select>
			<br /><br />
			<label for="message">Your Message</label>
			<textarea rows="3" id="message" name="message"></textarea>
			<br /><br />
			<p style="text-align: right;" class="rq_p"><input type="button" id="rsend" value="Send" onClick="sendRequestInfo()" /></p>
		</tr>
	</table>
</div>

<script src="<?php echo plugins_url('assets/js/jquery-1.11.3.min.js',dirname(__FILE__)); ?>"></script> 
<script>
	function loadinfocontent(){
		$.post(
			"<?php echo plugins_url('frontend/trans/trans_slides.php',dirname(__FILE__)); ?>?funct=get_getinfo_content",
			{subcode: '<?php echo $_GET['subcode']; ?>'},
		    function(data){
		        $('#info-contact').html(data.info_content + data.newsform);
		    },
		    'json'
		);	
	}
	
	function sendRequestInfo(){
		
		$('#rsend').val('Sending Request...');
		
		var rfname = $('#fname').val();
		var rlname = $('#fname').val();
		var rphone = $('#phone').val();
		var remail = $('#email').val();
		var rtsource = $('#traffic_source').val();
		var roccup = $('#occupation_source').val();
		var rfamsize = $('#family_size_source').val();
		var rmessage = $('#message').val();
		
		if(rfname.length == 0 || rlname.length == 0 || rphone.length == 0 || remail.length == 0){
			alert('Please fill all the required fields.');
			$('#rsend').val('Send');
			return;	
		}
		
		$.post(
			"<?php echo plugins_url('frontend/trans/trans_slides.php',dirname(__FILE__)); ?>?funct=requestinformation",
			{
				fname: rfname,
				lname: rlname,
				phone: rphone,
				email: remail,
				tsource: rtsource,
				occup: roccup,
				famsize: rfamsize,
				message: rmessage
			},
		    function(data){
			    if(data.success){
				    
				    alert(data.msg);
				    $('#fname').val('');
					$('#lname').val('');
					$('#phone').val('');
					$('#email').val('');
					$('#message').val('');					
					$('#rsend').val('Send');
					
			    }else{
				    alert(data.msg);
				    $('#rsend').val('Send');
			    }
		    },
		    'json'
		);	
	}
	
	function sendSubscription(){
		
		$('#ssignup').val('Sending Request...');
		
		var semail = $('#subscribenews').val();
		
		if(semail.length == 0){
			alert('Please enter valid email.');
			$('#ssignup').val('Sign Up');
			return;	
		}
		
		$.post(
			"<?php echo plugins_url('frontend/trans/trans_slides.php',dirname(__FILE__)); ?>?funct=news_subscription",
			{
				email: semail
			},
		    function(data){
			    if(data.success){
				    
				    alert(data.msg);
				    $('#subscribenews').val('');				
					$('#ssignup').val('Sign Up');
					
			    }else{
				    alert(data.msg);
				    $('#ssignup').val('Sign Up');
			    }
		    },
		    'json'
		);	
	}
	
	loadinfocontent();
</script>