<div style="width: 100%; overflow: auto;">
	<div id="moveinready-data"><center><h4>Loading Data...</h4></center></div>
</div>

<script src="<?php echo plugins_url('assets/js/jquery-1.11.3.min.js',dirname(__FILE__)); ?>"></script> 
<script>

	function loadSubdivisionmoveinready(){
		$.post(
			"<?php echo plugins_url('frontend/trans/trans_phaselot.php',dirname(__FILE__)); ?>?funct=fetch_moveinreadyhouse&page=moveinreadyhouse&subcode=<?php echo $_GET['subcode']; ?>",
			function(data){
				 $('#moveinready-data').html(data.htmldata);
			},
			'json'
		);	
	}
			
	loadSubdivisionmoveinready();
</script>