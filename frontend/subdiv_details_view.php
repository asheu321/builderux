<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&amp;language=en"></script>
<script src="<?php echo plugins_url('assets/js/gmap3.min.js',dirname(__FILE__)); ?>"></script>
<style>
#map-canvas {
     height: 350px;
     margin: 0px;
     padding: 0px
}
	
#map-canvas img 
{
    max-width: none;
}
</style>
<div style="width: 100%; overflow: auto;" id="data_result">
	<center><h4>Loading Floorplan...</h4></center>
</div>

<script src="<?php echo plugins_url('assets/js/jquery-1.11.3.min.js',dirname(__FILE__)); ?>"></script> 
 
<script>

	var geocoder;
	var map;
	
	function loadSubdivisionfloorplan(){
		$.post(
			"<?php echo plugins_url('frontend/trans/trans_subdivision.php',dirname(__FILE__)); ?>?funct=fetch_subdivsion_detail&page=subdivisiondetails&code=<?php echo $_GET['subcode']; ?>",
			{subcode: '<?php echo $_GET['subcode']; ?>'},
		    function(data){
		        $('#data_result').html(data.info);
		        initialize(data.lat,data.long,data.title);
		    },
		    'json'
		);	
	}
	
	function initialize(flat,flong,fname) {
		  geocoder = new google.maps.Geocoder();
		  var latlng = new google.maps.LatLng(flat,flong);
		  var mapOptions = {
		    zoom: 14,
		    center: latlng
		  }
		  map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

  		 setMarkers(map,latlng,fname);
	}
	
	function setMarkers(map,latlng,title) {
		  
		  var image = {
		    size: new google.maps.Size(20, 32),
		    // The origin for this image is 0,0.
		    origin: new google.maps.Point(0,0),
		    // The anchor for this image is the base of the flagpole at 0,32.
		    anchor: new google.maps.Point(0, 32)
		  };
		  
		  var pinIcon = new google.maps.MarkerImage(
			    "<?php echo plugins_url('assets/images/map.png',dirname(__FILE__)); ?>",
			    null, /* size is determined at runtime */
			    null, /* origin is 0,0 */
			    null, /* anchor is bottom center of the scaled image */
			    new google.maps.Size(68, 68)
			);  
		  
		  var shape = {
		      coord: [1, 1, 1, 20, 18, 20, 18 , 1],
		      type: 'poly'
		  };
		 
		  var marker = new google.maps.Marker({
		        position: latlng,
		        map: map,
		        shape: shape,
		        title: title,
		        zIndex: 999,
		        icon: pinIcon
		   });
	}
	
	loadSubdivisionfloorplan();
</script>