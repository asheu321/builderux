<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&amp;language=en"></script>
<script src="<?php echo plugins_url('assets/js/gmap3.min.js',dirname(__FILE__)); ?>"></script>
<style>
#bux-overlay {
	 background-color: #d1d1d1;
     visibility: hidden;
     position: fixed;
     left: 0px;
     top: 0px;
     width:100%;
     height:100%;
     text-align:center;
     z-index: 1000;
     overflow:auto;
     padding-bottom: 10px;
}

#bux-div {
     width: 90%;
     height: auto;
     position: absolute;
     margin: 5%;
     background-color: #fff;
     border:1px solid #000;
     padding:15px;
     text-align:center;
     font-size: 12px;
}

#bux-options {
     width: 70%;
     height: auto;
     position: absolute;
     margin: 15%;
     background-color: #fff;
     border:1px solid #000;
     padding:15px;
     text-align:center;
     font-size: 12px;
}

.bux-input {
	font-size: 12px;	
}

#map-canvas {
     height: 400px;
     margin: 0px;
     padding: 0px
}
	
#map-canvas img 
{
    max-width: none;
}
</style>
<div style="width: 100%; overflow: auto;">
	<p style="text-align: right;" id="loginstatus">
	<?php 
		@session_start(); 
		if(@$_SESSION['islogin']){
			echo 'Account: '.$_SESSION['useremail'].' | <a href="javascript: void(0)" onClick="logMeOut()">Logout</a>';
		}
		
	?>
	</p>
	<table>
		<tr>
			<td id="step1">1. Select Subdivision</td>
			<td id="step2">2. Select Plan</td>
			<td id="step3">3. Select Unit</td>
			<td id="step4">4. Select Elevation</td>
			<td id="step5">5. Select Option</td>
			<td id="step6">6. Send to Sales Agent</td>
		</tr>
	</table>
	<div class="map" id="map-canvas"></div>
	<div id="step-html">
		<center><h4>Loading Data...</h4></center>
	</div>
</div>

<div id="bux-overlay">
     <div id="bux-div">
     	  <p style="text-align: right; font-size: 12px; font-weight: bold;"><a href="javascript: void(0)" onClick="overlay()">Close</a></p>
          <p style="font-size: 15px;"><strong>Please create an account OR login to continue.</strong></p>
          <div style="padding: 15px;width: 50%; float: left; height: auto; border-right: solid 1px #ccc; position: relative; text-align: left;" class="cur_reg">
          	<p style="font-size: 13px; padding-bottom: 5px; border-bottom: solid 1px #ccc;"><strong>New Customer</strong></p>
          	<form id="user_reg_form" class="form_customer">
          	<p>
          		First Name*
          		<br />
          		<input type="text" name="reg_firstname" id="reg_firstname" class="bux-input">
          	</p>
          	<p>
          		Last Name*
          		<br />
          		<input type="text" name="reg_lastname" id="reg_lastname" class="bux-input">
          	</p>
          	<p>
          		Phone No.*
          		<br />
          		<input type="text" name="reg_phone" id="reg_phone" class="bux-input">
          	</p>
          	<p>
          		Email*
          		<br />
          		<input type="text" name="reg_email" id="reg_email" class="bux-input">
          	</p>
          	<p>
          		Password*
          		<br />
          		<input type="password" name="reg_password" id="reg_password" class="bux-input">
          	</p>
          	<p id="reg_error" style="color: red;">&nbsp;</p>
          	<p><input type="button" value="Register" onClick="registerUser()"></p>
          	</form>
          </div>
          
          <div style="padding: 15px;width: 50%; float: left; height: auto; position: relative; text-align: left;" class="cur_customer">
          	<p style="font-size: 13px; padding-bottom: 5px; border-bottom: solid 1px #ccc;"><strong>Current Customer</strong></p>
          	<p>
          		Username / Email
          		<br />
          		<input type="text" name="log_username" id="log_username" class="bux-input">
          	</p>
          	<p>
          		Password
          		<br />
          		<input type="password" name="log_password" id="log_password" class="bux-input">
          	</p>
          	<p id="log_error" style="color: red;">&nbsp;</p>
          	<p><input type="button" value="Login" onClick="login()"></p>
          </div>
     </div>
     <div id="bux-options"></div>   
</div>

<script src="<?php echo plugins_url('assets/js/jquery-1.11.3.min.js',dirname(__FILE__)); ?>"></script>
<script>

	var geocoder;
	var map;
	var infoWindow = null;
	var beaches = [];
	var markersArray = new Array();
	
	function getMapInfo(){
		$('#step1').css("background-color", "#2aacff");
		
		$.post(
			"<?php echo plugins_url('frontend/trans/trans_map.php',dirname(__FILE__)); ?>?funct=fetch_mainmap&page=choose_your_home&orderby=linkid",
		    function(data){
		        initBeaches(data.mapinfo);
		    },
		    'json'
		);	
	}
	
	function initialize(flat,flong,beach) {
		  geocoder = new google.maps.Geocoder();
		  var latlng = new google.maps.LatLng(flat,flong);
		  var mapOptions = {
		    zoom: 3,
		    center: latlng
		  }
		  
		  infoWindow = new google.maps.InfoWindow({content:"holding..."});
		  
		  map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
		  google.maps.event.trigger(map, 'resize');
  		  setMarkers(map,beach);
	}
	
	function initBeaches(infos){
		
		for(var x=0; x<=(infos.length - 1); x++){
			var temp = infos[x];
			beaches.push([temp.name,temp.latitude,temp.longitude,temp.linkid,temp.linkid,temp.name,temp.logourl,temp.price_range,temp.marketingdescription]);
		}
		
		initialize(infos[0].latitude,infos[0].longitude,beaches);
		
	}
	
	function setMarkers(map, locations) {
		  
		  var pinIcon = new google.maps.MarkerImage(
			    "<?php echo plugins_url('assets/images/map.png',dirname(__FILE__)); ?>",
			    null, /* size is determined at runtime */
			    null, /* origin is 0,0 */
			    null, /* anchor is bottom center of the scaled image */
			    new google.maps.Size(68, 68)
			);  
		 
		 
		  for (var i = 0; i < locations.length; i++) {
		  	
		  	var beach = locations[i];
		  	
		  	  var mycontent = "<div style='width: 600px;'>"+
			    				"<div style='width: 200px; float: left !important;'><img style='width: 190px;' src='"+beach[6]+"' alt='"+beach[0]+"' /></div>"+
			    		 		"<div class='map_desc' style='width: 384px; float: right !important;'>"+
			    		 			"<h6 style='margin:0px !important'>"+beach[0]+"</h6>"+
			    		 			"<hr  style='margin:0px !important'><p class='pop-desc'>"+
			    		 			beach[8]+
			    		 			"</p>"+
			    		 			"Price Range: $"+beach[7]+
			    		 			"<br /><button class='btn btn-primary' onclick=chooseNext(1,"+beach[3]+")>View Details</button>"+
			    		 			"</div>"+
			    		 "</div>";
		  	
		  	
		  	var  myLatLng = new google.maps.LatLng(beach[1], beach[2]);
		  	addMarker(myLatLng,beach[0],mycontent,pinIcon);
			
		  }
	}
	
	function addMarker(m_position,m_title,m_infowindow,icon) {
		var mark;
		if (markersArray.length!=0) 
		{
			duplicate = false;
			var markcopy;
			var markersCopy = [];
			while(markcopy=markersArray.pop())
			{
				if((markcopy.position.lat()==m_position.lat())&&(markcopy.position.lng()==m_position.lng())) duplicate = true;
				markersCopy.push(markcopy);
			}
			markersArray = markersCopy;
			if(duplicate==false)
			{
				marker = new google.maps.Marker({
				  	position: m_position,
				  	map: map,
				  	icon:icon,
					title: m_title
				});
				markersArray.push(marker);
				mark = markersArray.pop();
				google.maps.event.addListener(mark, 'click', function() {
					infoWindow.open(map,mark);
					var stringContent = m_infowindow;
	 				infoWindow.setContent(stringContent);
	 				infoWindow.open(map, this);
					
				});
				markersArray.push(mark);
			}
		}
		else
		{
			marker = new google.maps.Marker({
			  	position: m_position,
			  	map: map,
			  	icon:icon,
				title: m_title
			});
			markersArray.push(marker);
			mark = markersArray.pop();
			google.maps.event.addListener(mark, 'click', function() {
				infoWindow.open(map,mark);
				var stringContent = m_infowindow;
	 			infoWindow.setContent(stringContent);
	 			infoWindow.open(map, this);
			});
			markersArray.push(mark);
		}
	}

	
	getMapInfo();
	
	
	var lot_total = 0;
	var optiondata = 0;
	var optSelected = {};
	optSelected[0] = {id: 0, status: false, count: 0, price: 0, desc: ""};
	var topodata;
	var iconcoord;

	var userdata = {
			global_subdivisionid: 0,
			global_planid: 0,
			global_unitid: 0,
			global_elevationid: 0,
			islogin: false,
			userid: ''
		};
		
	var stepref = new Array();
	stepref[1] = true;
	stepref[2] = false;
	stepref[3] = false;
	stepref[4] = false;
	stepref[5] = false;
	stepref[6] = false;

	
	function chooseNext(stepid,dataid){
		if(stepid == 1){
			$('#step2').css("background-color", "#2aacff");
			userdata.global_subdivisionid = dataid;
			step2(userdata.global_subdivisionid);
			$('#map-canvas').hide();
		}
		
		if(stepid == 2){
			if(userdata.islogin){
				userdata.global_planid = dataid;
				$.post(
					"<?php echo plugins_url('frontend/trans/trans_subdivisionplan.php',dirname(__FILE__)); ?>?funct=direct_unit_link&page=choose_your_home",
					{subid: userdata.global_subdivisionid},
				    function(data){
					    $('#step3').css("background-color", "#2aacff");
				        
				        topodata = data.topodata;
					    $('#map-canvas').show();
					    $('#map-canvas').css("height","auto");
					    $('#map-canvas').html(data.topo_img);
					    $('#step-html').html(data.htmldata);
				    },
				    'json'
				);
			}else{
				userdata.global_planid = dataid;
				overlay();
				$('#bux-options').hide();
				$('#bux-optionsimg').hide();
			}
		}
		
		if(stepid == 3){
			$('#step4').css("background-color", "#2aacff");
			userdata.global_unitid = dataid;
			step3(userdata.global_unitid);
		}
		
		if(stepid == 4){
			$('#step5').css("background-color", "#2aacff");
			userdata.global_elevationid = dataid;
			step4(userdata.global_elevationid);
		}
		
		if(stepid == 5){
			$('#step6').css("background-color", "#2aacff");

			step5(userdata);
		}
		
	}
	
	function step1(){
		$('#step1').css("background-color", "#2aacff");
		
		$.post(
			"<?php echo plugins_url('frontend/trans/trans_subdivision.php',dirname(__FILE__)); ?>?funct=fetch_subdivision&page=choose_your_home",
		    function(data){
		        $('#step-html').html(data.subdivision);
		    },
		    'json'
		);	
	}
	
	function step2(subdivisionid){
		$.post(
			"<?php echo plugins_url('frontend/trans/trans_subdivisionplan.php',dirname(__FILE__)); ?>?funct=fetch_subdivisionplan&page=choose_your_home",
			{subid: subdivisionid},
		    function(data){
			    userdata.islogin = data.islogin;
		        $('#step-html').html(data.plan);
		    },
		    'json'
		);
	}
	
	function step3(unitid){
		$.post(
			"<?php echo plugins_url('frontend/trans/trans_subdivisionplan.php',dirname(__FILE__)); ?>?funct=fetch_elevation&page=choose_your_home",
			{unitid: unitid},
		    function(data){
			    $('#map-canvas').html('');
			    $('#map-canvas').hide();
		        $('#step-html').html(data.htmldata);
		    },
		    'json'
		);
	}
	
	function step4(elevationid){
		$.post(
			"<?php echo plugins_url('frontend/trans/trans_subdivisionplan.php',dirname(__FILE__)); ?>?funct=fetch_lotoptions&page=choose_your_home",
			{elevationid: elevationid, planid: userdata.global_planid, unitid: userdata.global_unitid},
		    function(data){
			    optiondata = data.optioncount;
			    
			    for(var dx=0; dx<=(optiondata.length - 1); dx++){
				   optSelected[optiondata[dx].id] = {id: optiondata[dx].id, status: false, count: 0, price: optiondata[dx].price, desc: ""};
			    }
			    
		        $('#step-html').html(data.htmldata);
		        $('#lotinfo-div').html(data.lotinfo);
		        lot_total = data.lotprice;

		    },
		    'json'
		);		
	}
	
	function step5(alldata){
		$.post(
			"<?php echo plugins_url('frontend/trans/trans_subdivisionplan.php',dirname(__FILE__)); ?>?funct=fetch_submitforreview&page=choose_your_home",
			{requestdata: userdata, options: optSelected},
		    function(data){
			    
		        $('#step-html').html(data.htmldata);
		    },
		    'json'
		);	
	}
	
	function overlay() {
		el = document.getElementById("bux-overlay");
		el.style.visibility = (el.style.visibility == "visible") ? "hidden" : "visible";
	}
	
	function login(){
		var luname = $('#log_username').val();	
		var lpassw = $('#log_password').val();
		if(luname.length > 0 && lpassw.length > 0){
			$.post(
			"<?php echo plugins_url('frontend/trans/trans_subdivisionplan.php',dirname(__FILE__)); ?>?funct=login&page=choose_your_home",
			{username: luname, password: lpassw, subdivisionid: userdata.global_subdivisionid},
		    function(data){
			    if(data.success){
				    userdata.islogin = true;
				    userdata.userid = data.email;
				    $('#step3').css("background-color", "#2aacff");
				    overlay();
				    $('#loginstatus').html('Account: '+ data.email +' | <a href="javascript: void(0)" onClick="logMeOut()">Logout</a>');
				    
				    topodata = data.topodata;
				    $('#map-canvas').show();
				    $('#map-canvas').css("height","auto");
				    $('#map-canvas').html(data.topo_img);
				    $('#step-html').html(data.htmldata);
			    }else{
				    $('#log_error').html(data.msg);
			    }
		    },
		    'json'
		);
		}else{
			$('#log_error').html('Please fill the username and password correctly.');
		}
	}
	
	function registerUser(){
		
		var rfname = $('#reg_firstname').val();
		var rlname = $('#reg_lastname').val();
		var rphone = $('#reg_phone').val();
		var remail = $('#reg_email').val();
		var rpass  = $('#reg_password').val();
		
		if(rfname.length > 0 && rlname.length > 0 && rphone.length > 0 && remail.length > 0 && rpass.length > 0){
			
			$.post(
				"<?php echo plugins_url('frontend/trans/trans_subdivisionplan.php',dirname(__FILE__)); ?>?funct=register_user&page=choose_your_home",
				{
					fname: rfname,
					lname: rlname,
					phone: rphone,
					email: remail,
					passw: rpass,
					subdivisionid: userdata.global_subdivisionid
					
				},
			    function(data){
			        if(data.success){
				        userdata.islogin = true;
				        userdata.userid = data.email;
				        $('#step3').css("background-color", "#2aacff");
				        overlay();
				        $('#loginstatus').html('Account: '+ data.email +' | <a href="javascript: void(0)" onClick="logMeOut()">Logout</a>');
				        
				        $('#map-canvas').show();
				    	$('#map-canvas').css("height","auto");
				    	$('#map-canvas').html(data.topo_img);
				        $('#step-html').html(data.htmldata);
				        
			        }else{
				        $('#reg_error').html(data.msg);	
			        }
			    },
			    'json'
			);
			
		}else{
			$('#reg_error').html('Please fill all the required fields.');	
		}
		
	}
	
	function calculateOption(optionid){
		
		if($('#qty_' + optionid).val() > 0){
			optSelected[optionid].status = true;
			optSelected[optionid].count = $('#qty_' + optionid).val();

		}else{
			alert("Zero (0) is not allowed, please uncheck the item if you don't want to add.");
			$('#qty_' + optionid).val(1);		
			optSelected[optionid].status = true;
			optSelected[optionid].count = $('#qty_' + optionid).val();
		}
		
		displayCalculation();
	}
	
	function addThisItem(optionid){
		
		
		if($('#option_' + optionid).is(':checked')){
			$('#qty_' + optionid).removeAttr('disabled');
			$('#qty_' + optionid).val(1);
			
			optSelected[optionid].status = true;
			optSelected[optionid].count = $('#qty_' + optionid).val();
			optSelected[optionid].desc = $('#option_' + optionid).val();

			checkOptionRequirement(optionid);
			
		}else{
			$('#option_' + optionid).prop('checked', false);
			$('#qty_' + optionid).attr('disabled', 'disabled');
			$('#qty_' + optionid).val(0);
			
			optSelected[optionid].status = false;
			optSelected[optionid].count = $('#qty_' + optionid).val();
		}
		
		displayCalculation();
	}
	
	function addThisItemChild(optionid){
		
		overlay();
		$('#option_' + optionid).prop('checked', true);
		
		if($('#option_' + optionid).is(':checked')){
			$('#qty_' + optionid).removeAttr('disabled');
			$('#qty_' + optionid).val(1);
			
			optSelected[optionid].status = true;
			optSelected[optionid].count = $('#qty_' + optionid).val();
			optSelected[optionid].desc = $('#option_' + optionid).val();

			checkOptionRequirement(optionid);
			
		}else{
			$('#qty_' + optionid).attr('disabled', 'disabled');
			$('#qty_' + optionid).val(0);
			
			optSelected[optionid].status = false;
			optSelected[optionid].count = $('#qty_' + optionid).val();
		}
		
		displayCalculation();
	}
	
	function displayCalculation(){
		var ftotal = 0;
		$('#optcalc tbody').html('');
		$.each(optSelected, function(key){	
			if(optSelected[key].status){
				$('#optcalc tbody').append('<tr style="background: #f1f1f1;"><td>'+ optSelected[key].desc +'</td><td>'+ (optSelected[key].count * optSelected[key].price).toFixed(2) +'</td></tr>');	
				ftotal = ftotal + (optSelected[key].count * optSelected[key].price);
			}
		});	
		
		$('#info-total').html('Total Option Price: <strong>$' + ftotal.toFixed(2) +'</strong>');
	}
	
	function viewTopoMap(urlimg,phaseidx){
		
		var imagestring = urlimg;

		if(imagestring.indexOf(".jpg") > 0 || imagestring.indexOf(".png") > 0 || imagestring.indexOf(".gif") > 0){
			$('#topo-mapper').html('<div id="image-map_'+ phaseidx +'"><img id="image_'+ phaseidx +'" src="'+ imagestring +'"></div>');	
							
		}else{
			$('#topo-mapper').html('<h4>No Image Found</h4>');
		}
		
		getAllUnit(phaseidx);
	}
	
	function getAllUnit(phaseid){
		$('#unitViewer').html('<center><h4>Loading Data...</h4></center>');
		$.post(
				"<?php echo plugins_url('frontend/trans/trans_subdivisionplan.php',dirname(__FILE__)); ?>?funct=get_unitlot_byphase&page=choose_your_home",
				{
					subdivisionid: userdata.global_subdivisionid,
					phaseid: phaseid
					
				},
			    function(data){
			        if(data.success){
				        $('#unitViewer').html(data.unitlot);
				        iconcoord = data.coordxy;
				        
				        var zoom = 1;
				        var  imgpos = $("#image_"+ phaseid).position();
						var imgpos2 = $("#img_"+ phaseid).offset();
						var h = parseFloat(660 * zoom);
						var w = parseFloat(660 * zoom);
						$("#img_"+ phaseid).css("height",h +'px');
						$("#img_"+ phaseid).css("width",w +'px');
					
						if(iconcoord.length > 0){
							for(xy=0; xy<=(iconcoord.length - 1); xy++){
								var myleft = parseInt(((((  iconcoord[xy].pointx ) / 15) - 10) * zoom) + imgpos.left);
								var mytop = parseInt(((((   iconcoord[xy].pointy ) / 15) -10) * zoom) + imgpos.top);
								$("#image-map_"+ phaseid).append('<p style="color:yellow;position:absolute;top:'+ mytop +'px;left:'+ myleft +'px"><i class="fa fa-home fa-2x" style="color: red; cursor: pointer; cursor: hand;" onClick="chooseNext(3,'+ iconcoord[xy].unitid +')"></i></p>');
							}
						}
			        }
			    },
			    'json'
			);
	}
	
	function checkOptionRequirement(optionid){
		$.post(
				"<?php echo plugins_url('frontend/trans/trans_subdivisionplan.php',dirname(__FILE__)); ?>?funct=fetch_required_option&page=choose_your_home",
				{
					subid: userdata.global_subdivisionid,
					optionid: optionid,
					planid: userdata.global_planid
					
				},
			    function(data){
			        if(data.success){
				        overlay();
				        $('#bux-div').hide();
				        $('#bux-optionsimg').hide();
				        $('#bux-options').show();
				        $('#bux-options').html(data.htmldetail);
			        }
			    },
			    'json'
			);
	}
	
	function deleteHomeDesign(idx){
		$.post(
				"<?php echo plugins_url('frontend/trans/trans_subdivisionplan.php',dirname(__FILE__)); ?>?funct=delete_home_design&page=choose_your_home",
				{
					designid: idx,
					userid: userdata.userid
					
				},
			    function(data){
			        if(data.success){
				        $('#step-html').html(data.htmldata);
			        }
			    },
			    'json'
			);
	}
	
	function getPamphlet(idx){
		window.open("<?php echo plugins_url('frontend/trans/trans_subdivisionplan.php',dirname(__FILE__)); ?>?funct=get_pamphlet&page=choose_your_home&idx=" + idx +"&userid=" + userdata.userid, '_blank');
	}
	
	function logMeOut(){
		$.post(
				"<?php echo plugins_url('frontend/trans/trans_subdivisionplan.php',dirname(__FILE__)); ?>?funct=logout",
			    function(data){
			        if(data.success){
				        window.location.href = data.link;
			        }
			    },
			    'json'
		);
			
	}
	
	step1();
</script>