<div style="width: 100%; overflow: auto;">
	<table style="width: 100%; 	table-layout: auto !important;" class="mhd_table">
		<tr><td colspan="2" style="width: 100%;"><h4 class="mhd_h4" style="margin-bottom: 10px; margin-top: 10px; text-align: left;"><?php echo $_GET['plan']; ?></h4></td></tr>
		<tr><td style="width: 70%;" class="mhd_td">
	  		
		  	<p id="desc" style="    border-bottom: solid 1px #ccc; padding-bottom: 10px;"></p>
		  	<p style="font-weight: bold; margin-bottom: 10px; margin-top: 10px;" class="pln_heading">Floor Plan Images</p>
		  	<p id="images"></p>
		</td><td style="width: 30%; vertical-align: top; padding: 0px;" class="mhd_tddetail"> 	
		  <div id="specs" class="mhd_specs">
		  <p style="font-weight: bold; margin-bottom: 10px; margin-top: 10px;">Available in these subdivisions:</p>
		  <div class="panel-body" id="atsubdiv"></div>
		  <p style="font-weight: bold; margin-bottom: 10px; margin-top: 10px;">Immediately Available On These Lots:</p>
		  <div class="panel-body" id="atavaillot"></div></div>
		</td></tr>
	</table>
</div>

<script src="<?php echo plugins_url('assets/js/jquery-1.11.3.min.js',dirname(__FILE__)); ?>"></script> 
<script>
	function loadSubdivisionfloorplan(){
		$.post(
			"<?php echo plugins_url('frontend/trans/trans_subdivisionplan.php',dirname(__FILE__)); ?>?funct=fetch_floorplan_detail&page=floorplan&plan=<?php echo $_GET['plan']; ?>",
			{subcode: '<?php echo $_GET['subcode']; ?>'},
		    function(data){
		        $('#desc').html(data.desc);
		        $('#images').html(data.images);
		        $('#specs').html(data.specs);
		        $('#atsubdiv').html(data.atsubdiv);
		        $('#atavaillot').html(data.atavaillot);
		    },
		    'json'
		);	
	}
	
	loadSubdivisionfloorplan();
</script>