<div style="width: 100%; overflow: auto;">
	<div id="modelhouse-data"><center><h4>Loading Data...</h4></center></div>
</div>

<script src="<?php echo plugins_url('assets/js/jquery-1.11.3.min.js',dirname(__FILE__)); ?>"></script> 
<script>

	function loadSubdivisionmodelhouse(){
		$.post(
			"<?php echo plugins_url('frontend/trans/trans_phaselot.php',dirname(__FILE__)); ?>?funct=fetch_modelhouse&page=modelhouse&subcode=<?php echo $_GET['subcode']; ?>",
			function(data){
				 $('#modelhouse-data').html(data.htmldata);
			},
			'json'
		);	
	}
			
	loadSubdivisionmodelhouse();
</script>