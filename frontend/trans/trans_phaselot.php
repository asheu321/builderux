<?php
include "../../../../../wp-config.php";
global $wpdb; 

$funct = $_GET['funct'];
$page = $_GET['page'];

if($funct == 'fetch_modelhouse' && $page == 'modelhouse')
{
	$data = array();
	if(strlen($_GET['subcode']) > 0){$condition = " AND c.subdivision_code = '".$_GET['subcode']."' ";}else{$condition = "";}
	$sql = "SELECT
			c.*,
			a.*,
			s.custportallogourl,
			s.marketingdescription,
			s.name,
			s.latitude,
			s.longitude,
			s.hoafees,
			a.id AS linkid,
			REPLACE(REPLACE(s.name,' ',''),'/','-') AS linkname,
			MD5(s.id) AS subdivision_id,
			(SELECT filename FROM builder_phaseplanattachment spa WHERE spa.phaseplan_id = c.id LIMIT 1) AS filename,
			(SELECT MIN(lotprice) FROM builder_phaselot WHERE lotstatus='Model' ) AS minlotprice,
			(SELECT MAX(lotprice) FROM builder_phaselot WHERE lotstatus='Model' ) AS maxlotprice,
			sm.attachmenturl 
			FROM builder_phaselot a
			LEFT JOIN builder_lotplanallowed b ON a.id = b.lot_id
			LEFT JOIN builder_subdivisionphase sp ON sp.id = a.phase_id
			LEFT JOIN builder_subdivisionplan c ON c.masterplanid = b.masterplanid
			LEFT JOIN builder_subdivision s ON s.code = c.subdivision_code
			LEFT JOIN builder_division sd ON sd.code = s.division_code
			LEFT JOIN builder_master sm ON sm.builder_code = sd.builder_code
			WHERE lotstatus='Model' ".$condition." AND s.show_subdivision='Y' AND c.plan_status='Active' 
			AND sp.phase_status='Active' AND s.show_subdivision='Y'";

	$result = $wpdb->get_results($sql);
	
	$html = '<table style="width: 100%;" class="mvin_table">';

	foreach($result as $key => $obj){
		$html .= '<tr class="mvin_tr"><td style="width: 30%;" class="mvin_table_td">
					<img src="http://www.salessimplicity.net/Livedemo/attachments/'.$obj->filename.'" style="width: 300px; height: 300px;" class="mvin_table_img">
				  </td><td class="mvin_table_td2">
					<h4 class="mvin_h4">'.$obj->address1.', '.$obj->city.', '.$obj->state.'</h4>
					<table class="mvin_second_table"><tr>						  	 
							  <td class="mvin_second_td"><span>BEDS:</span> <span>'.$obj->bedrooms.'</span></td>
							  <td class="mvin_second_td"><span>BATHS:</span> <span>'.$obj->baths.'</span></td>
							  <td class="mvin_second_td"><span>STORIES:</span> <span>'.$obj->stories.'</span></td>
							  <td class="mvin_second_td"><span>GARAGE:</span> <span>0</span></td>
							  <td class="mvin_second_td"><span>SQFT:</span> <span>'.$obj->basesqft.'</span></td>
				   </tr></table>
				   <p><a href="'.get_site_url().'/builderux-model-house-details/?mpid='.$obj->masterplanid.'&linkid='.$obj->linkid.'" class="mvin_view">View Details</a></p>
				   <p style="border-top: dotted 1px #ccc; padding-top: 20px;" class="mvin_lot_details"><strong>Lot Detail</strong><br />'.addslashes(htmlspecialchars($obj->restriction, ENT_QUOTES)).'</p>
				   <p class="mvin_desc"><strong>Plan Description</strong><br />'.addslashes(htmlspecialchars($obj->description, ENT_QUOTES)).'</p>
				</td></tr>';
	}

	$html .= '</table>';

	die(json_encode(array('htmldata' => $html)));
}

if($funct == 'fetch_moveinreadyhouse' && $page == 'moveinreadyhouse')
{
	$data = array();
	if(strlen($_GET['subcode']) > 0){$condition = " AND c.subdivision_code = '".$_GET['subcode']."' ";}else{$condition = "";}
	$sql = "SELECT
			c.*,
			a.*,
			s.custportallogourl,
			s.marketingdescription,
			s.name,
			s.latitude,
			s.longitude,
			s.hoafees,
			a.id AS linkid,
			REPLACE(REPLACE(s.name,' ',''),'/','-') AS linkname,
			MD5(s.id) AS subdivision_id,
			(SELECT filename FROM builder_phaseplanattachment spa WHERE spa.phaseplan_id = c.id LIMIT 1) AS filename,
			(SELECT MIN(lotprice) FROM builder_phaselot WHERE lotstatus='Model' ) AS minlotprice,
			(SELECT MAX(lotprice) FROM builder_phaselot WHERE lotstatus='Model' ) AS maxlotprice,
			sm.attachmenturl 
			FROM builder_phaselot a
			LEFT JOIN builder_lotplanallowed b ON a.id = b.lot_id
			LEFT JOIN builder_subdivisionphase sp ON sp.id = a.phase_id
			LEFT JOIN builder_subdivisionplan c ON c.masterplanid = b.masterplanid
			LEFT JOIN builder_subdivision s ON s.code = c.subdivision_code
			LEFT JOIN builder_division sd ON sd.code = s.division_code
			LEFT JOIN builder_master sm ON sm.builder_code = sd.builder_code
			WHERE lotstatus='Spec' ".$condition." AND s.show_subdivision='Y' AND c.plan_status='Active' 
			AND sp.phase_status='Active' AND s.show_subdivision='Y'";

	$result = $wpdb->get_results($sql);
	
	$html = '<table style="width: 100%;" class="mvin_table">';

	foreach($result as $key => $obj){
		$html .= '<tr class="mvin_tr"><td style="width: 30%;" class="mvin_table_td">
					<img src="http://www.salessimplicity.net/Livedemo/attachments/'.$obj->filename.'" style="width: 300px; height: 300px;" class="mvin_table_img">
				  </td><td class="mvin_table_td2">
					<h4 class="mvin_h4">'.$obj->address1.', '.$obj->city.', '.$obj->state.'</h4>
					<table class="mvin_second_table"><tr>						  	 
							  <td class="mvin_second_td"><span>BEDS:</span> <span>'.$obj->bedrooms.'</span></td>
							  <td class="mvin_second_td"><span>BATHS:</span> <span>'.$obj->baths.'</span></td>
							  <td class="mvin_second_td"><span>STORIES:</span> <span>'.$obj->stories.'</span></td>
							  <td class="mvin_second_td"><span>GARAGE:</span> <span>0</span></td>
							  <td class="mvin_second_td"><span>SQFT:</span> <span>'.$obj->basesqft.'</span></td>
				   </tr></table>
				   <p><a href="'.get_site_url().'/builderux-movein-ready-details/?mpid='.$obj->masterplanid.'&linkid='.$obj->linkid.'" class="mvin_view">View Details</a></p>
				   <p style="border-top: dotted 1px #ccc; padding-top: 20px;" class="mvin_lot_details"><strong>Lot Detail</strong><br />'.addslashes(htmlspecialchars($obj->restriction, ENT_QUOTES)).'</p>
				   <p class="mvin_desc"><strong>Plan Description</strong><br />'.addslashes(htmlspecialchars($obj->description, ENT_QUOTES)).'</p>
				</td></tr>';
	}

	$html .= '</table>';

	die(json_encode(array('htmldata' => $html)));
}

if($funct == 'fetch_moveinreadyhouse_detail' && $page == 'moveinreadyhouse')
{
	$mpid = $_GET['mpid'];
	$linkid = $_GET['linkid'];
	$info = '';
	
	$details = get_lothome_details($linkid,$mpid);
	$lotid = $details[0]->lotid;	
	$attachement = get_lot_attachment($lotid);
	$lotoption = get_lotoptions($linkid,$mpid);
	$phaseplanattachment = get_planattachment_bylot($linkid,$mpid);
	
	//echo "<pre>"; print_r($details); die();
	
	$info = '<table class="mhd_tbl">';
	$info .= '<tr><td colspan="2" class="mhd_tbd">Block #: '.$details[0]->blocknum.'</td></tr>';
	$info .= '<tr><td class="mhd_tbd">Subdivision Name</td><td class="mhd_tbd">'.$details[0]->name.'</td></tr>';
	$info .= '<tr><td class="mhd_tbd">Spec Price</td><td class="mhd_tbd">$'.$details[0]->lotprice.'</td></tr>';
	//$info .= '<tr><td>SQ FT.</td><td>'.$details[0]->blocknum.'</td></tr>';
	$info .= '<tr><td class="mhd_tbd">Bedrooms</td><td class="mhd_tbd">'.$details[0]->bedrooms.'</td></tr>';
	$info .= '<tr><td class="mhd_tbd">Bathrooms</td><td class="mhd_tbd">'.$details[0]->baths.'</td></tr>';
	$info .= '</table>';
	
	if(count($lotoption) > 0){
		$opt = '<p style="font-weight: bold; margin-bottom: 10px; margin-top: 10px;" class="mhd_opt">Options</p><table>';
		foreach($lotoption as $key => $obj){
			$opt .= '<tr><td>'.$obj->optiondesc.'</td><td>'.$obj->unitprice.'</td></tr>';
		}
		$opt .= '</table>';
	}
	
	$lotimages= '';
	foreach($attachement as $key => $obj){
		if(strlen($obj->attachmenturl) > 0 && strlen($obj->filename) > 0){
			$lotimages .= '<div style="background: #ccc; border: solid 5px #ccc; float: left; margin-bottom: 10px; margin-right: 10px;" class="mhd_dvimg"><img src="'.$obj->attachmenturl.'/'.$obj->filename.'" style="width: 350px;"></div>';
		}
	}
	
	$fplanimages = '';
	if(count($phaseplanattachment['FlorPlan']) > 0){
		foreach($phaseplanattachment['FlorPlan'] as $key => $obj){
			if(strlen($obj->attachmenturl) > 0 && strlen($obj->filename) > 0){
				$fplanimages .= '<div style="background: #ccc; border: solid 5px #ccc; float: left; margin-bottom: 10px; margin-right: 10px;" class="mhd_dvimg"><img src="'.$obj->attachmenturl.'/'.$obj->filename.'" style="width: 350px;"></div>';
			}
		}
	}
	
	$mplanimages = '';
	if(count($phaseplanattachment['MasterPlan']) > 0){
		foreach($phaseplanattachment['MasterPlan'] as $key => $obj){
			if(strlen($obj->attachmenturl) > 0 && strlen($obj->filename) > 0){
				$mplanimages .= '<div style="background: #ccc; border: solid 5px #ccc; float: left; margin-bottom: 10px; margin-right: 10px;" class="mhd_dvimg"><img src="'.$obj->attachmenturl.'/'.$obj->filename.'" style="width: 350px;"></div>';
			}
		}
	}
	
	die(
		json_encode(
			array(
				'success' => true,
				'address' => $details[0]->address1.', '.$details[0]->city.', '.$details[0]->state,
				'lotimages' => $lotimages,
				'fplanimages' => $fplanimages,
				'mplanimages' => $mplanimages,
				'info' => $info,
				'opt' => $opt,
				'restriction' => $details[0]->restriction
			)
		)
	);
}


function get_lothome_details($id,$masterplanid){
	
	global $wpdb; 
	
	$sql = "SELECT a.*,a.id AS lotid,s.*,splan.baths,splan.bedrooms,splan.subdivision_code,
			(SELECT zip FROM builder_salesoffice sso WHERE sso.subdivision_code=sp.subdivision_code LIMIT 1) AS zip
				FROM builder_phaselot a 
				LEFT JOIN builder_lotplanallowed spl ON spl.lot_id = a.id AND spl.masterplanid = $masterplanid	
				LEFT JOIN builder_subdivisionphase sp ON sp.id = a.phase_id
				LEFT JOIN builder_subdivision s ON s.code = sp.subdivision_code
				LEFT JOIN builder_subdivisionplan splan ON splan.subdivision_code = s.code AND splan.masterplanid = $masterplanid
				WHERE a.id = $id";	
				
	$details = $wpdb->get_results($sql);
	
	return $details;
}

function get_lot_attachment($id){
	
	global $wpdb; 
	
	$sql = "SELECT a.*,f.attachmenturl FROM builder_lotattachment a
   				LEFT JOIN builder_phaselot b ON a.lotid = b.id
   				LEFT JOIN builder_subdivisionphase c ON b.phase_id = c.id
   				LEFT JOIN builder_subdivision d ON c.subdivision_code = d.code
   				LEFT JOIN builder_division e ON e.code = d.division_code
   				LEFT JOIN builder_master f ON e.builder_code =f.builder_code
  				WHERE lotid = $id ";
  				
  	$details = $wpdb->get_results($sql);
	
	return $details;
}

function get_lotoptions($id,$masterplanid)
{
	global $wpdb;
	$sql1 = "SELECT subdivision_code FROM builder_phaselot spl 
			 LEFT JOIN builder_subdivisionphase sp ON sp.id=spl.phase_id
			 WHERE spl.id = $id";	
	$result = $wpdb->get_results($sql1);
	$subdivision_code = $result[0]->subdivision_code;
	
	$sql2 = "SELECT * FROM builder_phaseplanoptions
			 WHERE phaseplan_id IN ( SELECT id FROM builder_subdivisionplan sp 
						WHERE subdivision_code = '$subdivision_code' AND masterplanid = $masterplanid) 
			 AND optioncode IN (SELECT optioncode FROM builder_lotoptions WHERE lot_id = $id) ORDER BY optiongroupname";
			 
	$details = $wpdb->get_results($sql2);
	return $details;
}

function get_planattachment_bylot($id,$masterplanid)
{
	global $wpdb;
	$sql = "SELECT sp.*,sm.attachmenturl FROM  builder_phaseplan spp LEFT JOIN builder_phaseplanattachment 
				sp ON spp.id = sp.phaseplan_id
				LEFT JOIN builder_subdivisionphase  sph ON sph.id = spp.phase_id
				LEFT JOIN builder_subdivision s ON s.code=sph.subdivision_code
				LEFT JOIN builder_division sd ON sd.code = s.division_code
				LEFT JOIN builder_master sm ON sm.builder_code = sd.builder_code
				WHERE phaseplanid IN ( SELECT phaseplanid FROM  builder_phaselot a LEFT JOIN 
				builder_lotplanallowed b ON a.id = b.lot_id  AND masterplanid = $masterplanid
				WHERE a.id = $id )";	
				
	$details = $wpdb->get_results($sql);
	$data = array();
    foreach($details as $key => $value){
		$data[$value->type][] = $value;
	}
    return $data;
}

?>