<?php
include "../../../../../wp-config.php";
global $wpdb; 

$funct = $_GET['funct'];
$page = $_GET['page'];

if($funct == 'fetch_mainmap')
{
	$orderby = $_GET['orderby'];
	if($orderby == 'linked'){$orderby = 'a.id';}
	if($orderby == 'name'){$orderby = 'a.name';}
	
	$sql = "SELECT 
				a.id AS linkid,
				(SELECT CONCAT(FORMAT(MIN(basesqft),0),' - ',FORMAT(MAX(basesqft),0)) FROM builder_subdivisionplan sb1 WHERE sb1.subdivision_code = a.code  ) AS sqft,
				(SELECT CONCAT(FORMAT(MIN(baseprice),2),' - ',FORMAT(MAX(baseprice),2)) FROM builder_subdivisionplan sb2  WHERE sb2.subdivision_code = a.code) AS price_range,
				(SELECT CONCAT(city,',',state) FROM builder_salesoffice b WHERE b.subdivision_code=a.code LIMIT 1 ) AS filter,
				a.*,
				REPLACE(REPLACE(REPLACE(REPLACE(a.name,'(',''),')',''),' ',''),'/','-') AS mypath
			FROM builder_subdivision a 
			WHERE show_subdivision='Y'  
			ORDER BY ".$orderby;
			
	$result = $wpdb->get_results($sql);
	
	foreach($result as $key => $val){
		$data[] = array(
			'linkid' => $val->linkid,
			'sqft' => $val->sqft,
			'price_range' => $val->price_range,
			'filter' => $val->filter,
			'division_code' => $val->division_code,
			'code' => $val->code,
			'logourl' => $val->custportallogourl,
			'name' => $val->name,
			'subleademail' => $val->subleadsemail,
			'scatteredlots' => $val->addmodscatteredlots,
			'exclude' => $val->exclude,
			'legalname' => $val->legalname,
			'country' => $val->country,
			'fax' => $val->fax,
			'hoafees' => $val->hoafees,
			'estpropertytax' => $val->estpropertytax,
			'maxloanamount' => $val->linkid,
			'miscellaneousfee' => $val->miscellaneousfee,
			'hoaname' => $val->hoaname,
			'hoatype' => $val->hoatype,
			'latitude' => $val->latitude,
			'longitude' => $val->longitude,
			'marketingdescription' => $val->marketingdescription,
			'xml_lock' => $val->xml_lock,
			'show_subdivision' => $val->show_subdivision,
			'topoimagepath' => $val->topoimagepath,
			'mypath' => $val->mypath
		);
	}
	
	die(json_encode(array("mapinfo" => $data, "siteurl" => get_site_url())));
}

function get_city()
{
	global $wpdb;
	$result = $wpdb->get_results("select city,zip,state,replace(city,' ','-') as link  from builder_salesoffice group by city ");
		
	return $result;
}

?>