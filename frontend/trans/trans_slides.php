<?php
include "../../../../../wp-config.php";
global $wpdb; 

$funct = $_GET['funct'];
$page = $_GET['page'];

if($funct == 'updatecel'){
	
	$field = $_POST['name'];
	$val = $_POST['value'];
	$uid = $_POST['pk'];

	$wpdb->update( 
		'builder_slides', 
		array( 
			$field => $val
		), 
		array( 'slide_id' => $uid )
	);
	
	return true;
}

if($funct == 'get_getinfo_content')
{
	$result = $wpdb->get_results("select * from builder_requestinfo_settings");
	
	$data = array(
		"info_content" => $result[0]->content,
		"newsform" => $result[0]->news_form_text
	);
	
	die(json_encode($data));
}

if($funct == 'requestinformation')
{
	$fname = addslashes($_POST['fname']);
	$lname = addslashes($_POST['lname']);
	$phone = addslashes($_POST['phone']);
	$email = addslashes($_POST['email']);
	$tsource = addslashes($_POST['tsource']);
	$occup = addslashes($_POST['occup']);
	$famsize = addslashes($_POST['famsize']);
	$message = addslashes($_POST['message']);
	
	if(filter_var($email, FILTER_VALIDATE_EMAIL) === false){ die(json_encode(array("success" => false, "msg" => "Email is not valid."))); }
	
	$wpdb->insert( 
		'builder_request_more_info', 
		array( 
			'first_name' => $fname, 
			'last_name' => $lname,
			'phone' => $phone, 
			'email_address' => $email,
			'traffic_source' => $tsource, 
			'family_size' => $famsize,
			'occupation' => $occup, 
			'message' => $message 
		) 
	); 
	
	die(json_encode(array("success" => true, "msg" => "Your request is successfully submitted, we will email you soon.")));
}

if($funct == 'news_subscription')
{
	$email = addslashes($_POST['email']);

	if(filter_var($email, FILTER_VALIDATE_EMAIL) === false){ die(json_encode(array("success" => false, "msg" => "Email is not valid."))); }
	
	$wpdb->insert( 
		'builder_news_subscriber', 
		array( 
			'email' => $email, 
			'status' => 1,
			'date_added' => @date('Y-m-d H:i:s') 
		) 
	); 
	die(json_encode(array("success" => true, "msg" => "Your request subscription is successfully submitted.")));
}

?>