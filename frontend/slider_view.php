<?php

require_once './wp-load.php';
global $wpdb; 

$page = get_page_by_title('Choose Your Home');

$result = $wpdb->get_results("select * from builder_slides");
?>

<div style="width: 100%; border: solid 2px #ccc; height: 400px;" class="slider_first_div">
<?php $cnt=0; foreach($result as $key => $obj): $cnt++; ?>
<div id="slide<?php echo $cnt; ?>" style="position: relative; height: 400px; background: url('<?php echo $obj->image_path; ?>'); background-repeat: no-repeat; background-size: 100% 100%; background-size: cover;"  class="slider_second_div">
	
	<div style="position: absolute; bottom: 0px; width: 100%;"  class="slider_third_div">
		<p style="text-align: center;"  class="slider_p"><input type="button" onClick="window.location='<?php echo get_permalink($page->ID); ?>'"  class="slider_btn"/></p>
		<br />
		<p id="scontent" style="width: 100%; margin-bottom: 0px; background: #000; opacity: 0.5; padding: 5px; color: #fff;"  class="slider_fourth_div">
			<?php echo $obj->slide_content; ?>
		</p>
	</div>
</div>
<?php endforeach; ?>

</div>
<script src="<?php echo plugins_url('assets/js/jquery-1.11.3.min.js',dirname(__FILE__)); ?>"></script> 
<script>
$("#slide3").fadeOut();
$("#slide2").fadeOut();
var intVar = 1;
setInterval(
	function(){ 
		intVar++;
		if(intVar == 1){
			$("#slide3").fadeOut(
				function(){
					$("#slide1").fadeIn("slow");
				}
			);
			
		} 
		
		if(intVar == 2){
			$("#slide1").fadeOut(
				function(){
					$("#slide2").fadeIn("slow");	
				}
			);
			
		} 
		
		if(intVar == 3){
			$("#slide2").fadeOut(
				function(){
					$("#slide3").fadeIn("slow");	
				}
			);
			
			intVar=0;
		} 
	}, 
8000);	

</script>