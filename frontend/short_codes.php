<?php

function get_slider_func()
{
	include 'slider_view.php';
}

function get_choose_home_func()
{
	include 'choose_home_view.php';	
}

function get_where_we_build_func()
{
	include 'where_we_build_view.php';	
}

function get_floorplan_funct()
{
	include 'floorplan_view.php';	
}

function get_modelhouse_funct()
{
	include 'modelhouse_view.php';	
}

function get_move_in_ready_funct()
{
	include 'move_in_ready_view.php';	
}

function get_floorplan_details_funct()
{
	include 'floorplan_details_view.php';	
}

function get_moveinready_details_funct()
{
	include 'moveinready_details_view.php';	
}

function get_modelhouse_details_funct()
{
	include 'modelhouse_details_view.php';	
}

function get_request_moreinfo_funct()
{
	include 'request_moreinfo_view.php';	
}

function get_subdiv_details_funct()
{
	include 'subdiv_details_view.php';	
}

function get_wherewe_build_funct()

{
	include 'where_we_build_shortcode.php';	
}


?>