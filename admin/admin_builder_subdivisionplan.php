<link href="<?php echo plugins_url('assets/css/bootstrap.min.css',dirname(__FILE__)); ?>" rel="stylesheet">
<link href="<?php echo plugins_url('assets/css/dataTables.bootstrap.min.css',dirname(__FILE__)); ?>" rel="stylesheet">
<link href="<?php echo plugins_url('assets/css/bootstrap-editable.css',dirname(__FILE__)); ?>" rel="stylesheet">
<style>
.mytextarea {
  width: 600px !important;
}
</style>
<div style="padding: 15px; margin-top: 60px;">
<div class="panel panel-primary">
	  <div class="panel-heading"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span> <b>Subdivision Plan Management</b></div>
	  <div class="panel-body">
			<table id="builder-subdivisionplan" class="table table-striped table-bordered" cellspacing="0" width="100%">
		        <thead>
		            <tr>
		                <th>Subdivision Code</th>
		                <th>Plan Name</th>
		                <th>Base Price</th>
		                <th>Description</th>
		            </tr>
		        </thead>
		 </table>
	 </div>
	</div>
</div>

<script src="<?php echo plugins_url('assets/js/jquery-1.11.3.min.js',dirname(__FILE__)); ?>"></script> 
<script src="<?php echo plugins_url('assets/js/jquery.dataTables.min.js',dirname(__FILE__)); ?>"></script>
<script src="<?php echo plugins_url('assets/js/dataTables.bootstrap.min.js',dirname(__FILE__)); ?>"></script>
<script src="<?php echo plugins_url('assets/js/bootstrap.min.js',dirname(__FILE__)); ?>"></script>
<script src="<?php echo plugins_url('assets/js/bootstrap-editable.min.js',dirname(__FILE__)); ?>"></script>
<script>
  $(document).ready(function() {
	  $.fn.editable.defaults.mode = 'popup';
	  
	  $('#builder-subdivisionplan').DataTable( 
	  		{
	        	"ajax": "<?php echo plugins_url('admin/trans/trans_builder_subdivisionplan.php',dirname(__FILE__)); ?>?funct=fetch",
		        "columns": [
		            { "data": "subdivision_code" },
		            { "data": "plan_name" },
		            { "data": "price" },
		            { "data": "description" }
		        ]
		    }
	  );
	  
	  $('#builder-subdivisionplan').on( 'draw.dt', function () {
	    $('.planname').editable();
	   	$('.description').editable({inputclass: 'mytextarea'});
	  });
  });
 
</script>