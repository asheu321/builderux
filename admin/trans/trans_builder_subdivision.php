<?php

include "../../../../../wp-config.php";
$con = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

$funct = $_GET['funct'];

if($funct == 'fetch'){
	
	$data = array();
	$sql = "select * from builder_subdivision";
	$result = mysqli_query($con,$sql);
	
	while ($obj = mysqli_fetch_object($result)){
		$data[] = array(
			'division_code' => $obj->division_code,
			'code' => $obj->code,
			'name' => '<a href="javascript: void(0)" id="name" class="name" data-type="text" data-pk="'.$obj->id.'" data-url="'.plugins_url('trans/trans_builder_subdivision.php',dirname(__FILE__)).'?funct=updatecel" data-title="Enter Name">'.$obj->name.'</a>',
			'marketingdescription' => '<a href="javascript: void(0)" id="marketingdescription" class="marketingdescription" data-type="textarea" data-pk="'.$obj->id.'" data-url="'.plugins_url('trans/trans_builder_subdivision.php',dirname(__FILE__)).'?funct=updatecel" data-title="Enter Description">'.$obj->marketingdescription.'</a>'
		);
	}
	
	die(json_encode(array('data' => $data)));
}

if($funct == 'updatecel'){
	
	$field = $_POST['name'];
	$val = $_POST['value'];
	$uid = $_POST['pk'];
	
	$sql = "update builder_subdivision set ".$field." = '".$val."' where id = ".$uid;
	$result = mysqli_query($con,$sql);
	
	return true;
}

?>