<?php

include "../../../../../wp-config.php";
$con = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

$funct = $_GET['funct'];

if($funct == 'fetch'){
	
	$data = array();
	$sql = "select * from builder_subdivisionplan";
	$result = mysqli_query($con,$sql);
	
	while ($obj = mysqli_fetch_object($result)){
		$data[] = array(
			'subdivision_code' => $obj->subdivision_code,
			'plan_name' => '<a href="javascript: void(0)" id="planname" class="planname" data-type="text" data-pk="'.$obj->id.'" data-url="'.plugins_url('trans/trans_builder_subdivisionplan.php',dirname(__FILE__)).'?funct=updatecel" data-title="Enter Name">'.$obj->planname.'</a>',
			'price' => $obj->baseprice,
			'description' => '<a href="javascript: void(0)" id="description" class="description" data-type="textarea" data-pk="'.$obj->id.'" data-url="'.plugins_url('trans/trans_builder_subdivisionplan.php',dirname(__FILE__)).'?funct=updatecel" data-title="Enter Description">'.$obj->description.'</a>'
		);
	}
	
	die(json_encode(array('data' => $data)));
}

if($funct == 'updatecel'){
	
	$field = $_POST['name'];
	$val = $_POST['value'];
	$uid = $_POST['pk'];
	
	$sql = "update builder_subdivisionplan set ".$field." = '".$val."' where id = ".$uid;
	$result = mysqli_query($con,$sql);
	
	return true;
}

?>