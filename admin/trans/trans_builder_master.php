<?php

include "../../../../../wp-config.php";
$con = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

$funct = $_GET['funct'];

if($funct == 'fetch'){
	
	$data = array();
	$sql = "select * from builder_master";
	$result = mysqli_query($con,$sql);
	
	while ($obj = mysqli_fetch_object($result)){
		$data[] = array(
			'builder_code' => $obj->builder_code,
			'state' => $obj->state,
			'name' => '<a href="javascript: void(0)" id="name" class="name" data-type="text" data-pk="'.$obj->id.'" data-url="'.plugins_url('trans/trans_builder_master.php',dirname(__FILE__)).'?funct=updatecel" data-title="Enter Name">'.$obj->name.'</a>',
			'attachmenturl' => $obj->attachmenturl,
			'email' => '<a href="javascript: void(0)" id="email" class="email" data-type="text" data-pk="'.$obj->id.'" data-url="'.plugins_url('trans/trans_builder_master.php',dirname(__FILE__)).'?funct=updatecel" data-title="Enter Email">'.$obj->email.'</a>'
		);
	}
	
	die(json_encode(array('data' => $data)));
}

if($funct == 'updatecel'){
	
	$field = $_POST['name'];
	$val = $_POST['value'];
	$uid = $_POST['pk'];
	
	$sql = "update builder_master set ".$field." = '".$val."' where id = ".$uid;
	$result = mysqli_query($con,$sql);
	
	return true;
}

if($funct == 'update_lead')
{
	$apisrc = $_POST['apisrc'];
	$guidval = $_POST['guid'];
	$fbid = $_POST['fbid'];
	$fbkey = $_POST['fbkey'];
	
	$sql = "update builder_lead_settings set api_source = '$apisrc', guid = '$guidval', fb_id = '$fbid', fb_key = '$fbkey' ";

	mysqli_query($con,$sql);
	die(json_encode(array('success' => true)));

}

?>