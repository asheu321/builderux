<?php

include "../../../../../wp-config.php";
global $wpdb; 

$funct = $_GET['funct'];

if($funct == 'fetchusers'){
	
	$data = array();
	$sql = "select * from builder_user";
	$result = $wpdb->get_results($sql);
	
	foreach($result as $key => $obj){
		
		if($obj->status == 1){ $st = "Active"; }else{ $st = "InActive"; }
		
		$data[] = array(
			'userid' => $obj->userid,
			'firstname' => $obj->firstname,
			'lastname' => $obj->lastname,
			'phone' => $obj->phone,
			'email' => $obj->email,
			'status' => '<a href="javascript: void(0)" id="status" class="status" data-type="select" data-pk="'.$obj->userid.'" data-url="'.plugins_url('trans/trans_builder_users.php',dirname(__FILE__)).'?funct=updatecel" data-title="Select Status">'.$st.'</a>',
			'date_added' => $obj->date_added
		);
	}
	
	die(json_encode(array('data' => $data)));
}

if($funct == 'updatecel'){
	
	$field = $_POST['name'];
	$val = $_POST['value'];
	$uid = $_POST['pk'];
	
	$wpdb->update(
		'builder_user', 
		array( 
			$field => $val
		), 
		array( 'userid' => $uid )
	);
	
	return true;
}

if($funct == 'fetchforreview_data'){
	$data = array();
	$sql = "select * from builder_choosehome_request";
	$result = $wpdb->get_results($sql);
	
	foreach($result as $key => $obj){
	
		$opt = json_decode($obj->options);
		$opthtm = '';
		for($x=0; $x<=(count($opt) - 1); $x++){
			$opthtm .= get_option_desc($opt[$x]->id).'<br />';
		}
		
		if($obj->is_granted == 1){ $st = "True"; }else{ $st = "False"; }
		
		$data[] = array(
			'email' => $obj->user_email,
			'subdivision' => get_subdivision($obj->subdivisionid),
			'plan' => get_plan($obj->planid),
			'unit' => get_unit($obj->unitid),
			'elevation' => get_elevation($obj->elevationid),
			'options' => $opthtm,
			'isgranted' => $st,
			'date_added' => $obj->date_added
		);
	}
	
	die(json_encode(array('data' => $data)));
}

function get_option_desc($id)
{
	global $wpdb;
	$rs = array();
	$info = $wpdb->get_results("SELECT optiondesc FROM `builder_phaseplanoptions` WHERE id = $id ");

	return $info[0]->optiondesc;
}

function get_subdivision($id)
{
	global $wpdb; 
	$result = $wpdb->get_results("select * from builder_subdivision where id = $id ");
	
	return $result[0]->name;
}

function get_plan($id)
{
	global $wpdb; 
	$result = $wpdb->get_results("select * from builder_subdivisionplan where id = $id ");
	
	return $result[0]->planname;
}

function get_unit($id)
{
	global $wpdb; 
	$result = $wpdb->get_results("select * from builder_phaselot where id = $id ");
	
	return '#'.$result[0]->lotunitnum;
}

function get_elevation($id)
{
	global $wpdb; 
	$result = $wpdb->get_results("select * from builder_elevation where elevationid = $id ");
	
	return '#'.$result[0]->title;
}

?>