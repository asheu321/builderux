<?php

include "../../../../../wp-config.php";
$con = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

$funct = $_GET['funct'];

if($funct == 'fetch'){
	
	$data = array();
	$sql = "select * from builder_division";
	$result = mysqli_query($con,$sql);
	
	while ($obj = mysqli_fetch_object($result)){
		$data[] = array(
			'builder_code' => $obj->builder_code,
			'code' => $obj->code,
			'name' => '<a href="javascript: void(0)" id="name" class="name" data-type="text" data-pk="'.$obj->id.'" data-url="'.plugins_url('trans/trans_builder_division.php',dirname(__FILE__)).'?funct=updatecel" data-title="Enter Name">'.$obj->name.'</a>',
			'leadsemail' => '<a href="javascript: void(0)" id="defaultleadsemail" class="email" data-type="text" data-pk="'.$obj->id.'" data-url="'.plugins_url('trans/trans_builder_division.php',dirname(__FILE__)).'?funct=updatecel" data-title="Enter Email">'.$obj->defaultleadsemail.'</a>'
		);
	}
	
	die(json_encode(array('data' => $data)));
}

if($funct == 'updatecel'){
	
	$field = $_POST['name'];
	$val = $_POST['value'];
	$uid = $_POST['pk'];
	
	$sql = "update builder_division set ".$field." = '".$val."' where id = ".$uid;
	$result = mysqli_query($con,$sql);
	
	return true;
}

?>