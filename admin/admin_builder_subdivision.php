<link href="<?php echo plugins_url('assets/css/bootstrap.min.css',dirname(__FILE__)); ?>" rel="stylesheet">
<link href="<?php echo plugins_url('assets/css/dataTables.bootstrap.min.css',dirname(__FILE__)); ?>" rel="stylesheet">
<link href="<?php echo plugins_url('assets/css/bootstrap-editable.css',dirname(__FILE__)); ?>" rel="stylesheet">
<style>
.mytextarea {
  width: 600px !important;
}
</style>
<div style="padding: 15px; margin-top: 60px;">
<div class="panel panel-primary">
	  <div class="panel-heading"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span> <b>Builder Subdivision Management</b></div>
	  <div class="panel-body">
			<table id="builder-subdivision" class="table table-striped table-bordered" cellspacing="0" width="100%">
		        <thead>
		            <tr>
		                <th>Divsion Code</th>
		                <th>Code</th>
		                <th>Name</th>
		                <th>Marketing Description</th>
		            </tr>
		        </thead>
		 </table>
	 </div>
	</div>
</div>

<script src="<?php echo plugins_url('assets/js/jquery-1.11.3.min.js',dirname(__FILE__)); ?>"></script> 
<script src="<?php echo plugins_url('assets/js/jquery.dataTables.min.js',dirname(__FILE__)); ?>"></script>
<script src="<?php echo plugins_url('assets/js/dataTables.bootstrap.min.js',dirname(__FILE__)); ?>"></script>
<script src="<?php echo plugins_url('assets/js/bootstrap.min.js',dirname(__FILE__)); ?>"></script>
<script src="<?php echo plugins_url('assets/js/bootstrap-editable.min.js',dirname(__FILE__)); ?>"></script>
<script>
  $(document).ready(function() {
	  $.fn.editable.defaults.mode = 'popup';
	  
	  $('#builder-subdivision').DataTable( 
	  		{
	        	"ajax": "<?php echo plugins_url('admin/trans/trans_builder_subdivision.php',dirname(__FILE__)); ?>?funct=fetch",
		        "columns": [
		            { "data": "division_code" },
		            { "data": "code" },
		            { "data": "name" },
		            { "data": "marketingdescription" }
		        ]
		    }
	  );
	  
	  $('#builder-subdivision').on( 'draw.dt', function () {
	    $('.name').editable();
	   	$('.marketingdescription').editable({inputclass: 'mytextarea'});
	  });
  });
 
</script>