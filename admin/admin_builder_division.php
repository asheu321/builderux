<link href="<?php echo plugins_url('assets/css/bootstrap.min.css',dirname(__FILE__)); ?>" rel="stylesheet">
<link href="<?php echo plugins_url('assets/css/dataTables.bootstrap.min.css',dirname(__FILE__)); ?>" rel="stylesheet">
<link href="<?php echo plugins_url('assets/css/bootstrap-editable.css',dirname(__FILE__)); ?>" rel="stylesheet">

<div style="padding: 15px;">
<div class="panel panel-primary">
	  <div class="panel-heading"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span> <b>Builder Division Management</b></div>
	  <div class="panel-body">
			<table id="builder-division" class="table table-striped table-bordered" cellspacing="0" width="100%">
		        <thead>
		            <tr>
		                <th>Builder Code</th>
		                <th>Code</th>
		                <th>Name</th>
		                <th>Default Leads Email/Contact</th>
		            </tr>
		        </thead>
		 </table>
	 </div>
	</div>
</div>

<script src="<?php echo plugins_url('assets/js/jquery-1.11.3.min.js',dirname(__FILE__)); ?>"></script> 
<script src="<?php echo plugins_url('assets/js/jquery.dataTables.min.js',dirname(__FILE__)); ?>"></script>
<script src="<?php echo plugins_url('assets/js/dataTables.bootstrap.min.js',dirname(__FILE__)); ?>"></script>
<script src="<?php echo plugins_url('assets/js/bootstrap.min.js',dirname(__FILE__)); ?>"></script>
<script src="<?php echo plugins_url('assets/js/bootstrap-editable.min.js',dirname(__FILE__)); ?>"></script>
<script>
  $(document).ready(function() {
	  $.fn.editable.defaults.mode = 'popup';
	  
	  $('#builder-division').DataTable( 
	  		{
	        	"ajax": "<?php echo plugins_url('admin/trans/trans_builder_division.php',dirname(__FILE__)); ?>?funct=fetch",
		        "columns": [
		            { "data": "builder_code" },
		            { "data": "code" },
		            { "data": "name" },
		            { "data": "leadsemail" }
		        ]
		    }
	  );
	  
	  $('#builder-division').on( 'draw.dt', function () {
	    $('.name').editable();
	   	$('.email').editable();
	  });
  });
 
</script>