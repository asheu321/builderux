<link href="<?php echo plugins_url('assets/css/bootstrap.min.css',dirname(__FILE__)); ?>" rel="stylesheet">

<div style="padding: 15px;">
	<div class="panel panel-primary">
	  <div class="panel-heading"><span class="glyphicon glyphicon-dashboard" aria-hidden="true"></span> <b>BuilderUX Dashboard</b></div>
	  <div class="panel-body">
	    <div class="row">
			<div class="col-xs-6 col-md-3"> 
				<a class="thumbnail" href="<?php echo admin_url(); ?>admin.php?page=admin_builder_master" style="padding: 10px; text-decoration: none !important;"> 
					<p><span class="glyphicon glyphicon-align-justify" aria-hidden="true"></span> <b>Builder Master Management</b></p>
					<p>Edit Basic Builder Level Informatio.</p>
				</a> 				
			</div>
			<div class="col-xs-6 col-md-3"> 
				<a class="thumbnail" href="<?php echo admin_url(); ?>admin.php?page=admin_builder_division" style="padding: 10px; text-decoration: none !important;"> 
					<p><span class="glyphicon glyphicon-align-justify" aria-hidden="true"></span> <b>Division Management</b></p>
					<p>Modify your Division Descriptions.</p>
				</a> 
			</div>
			<div class="col-xs-6 col-md-3"> 
				<a class="thumbnail" href="<?php echo admin_url(); ?>admin.php?page=admin_builder_subdivision" style="padding: 10px; text-decoration: none !important;"> 
					<p><span class="glyphicon glyphicon-align-justify" aria-hidden="true"></span> <b>Subdivision Management</b></p>
					<p>Modify your subdivision descriptions pricing<br /> add custom media.</p>
				</a> 
			</div>
			<div class="col-xs-6 col-md-3"> 
				<a class="thumbnail" href="<?php echo admin_url(); ?>admin.php?page=admin_builder_subdivisionplan" style="padding: 10px; text-decoration: none !important;"> 
					<p><span class="glyphicon glyphicon-align-justify" aria-hidden="true"></span> <b>Subdivision Plan Management</b></p>
					<p>Modify your floor plan descriptions<br /> add media to your floor plans<br /> modify your options and their pricing.</p>
				</a> 
			</div>
		</div> 
	  </div>
	</div>
</div>

	