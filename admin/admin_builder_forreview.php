<link href="<?php echo plugins_url('assets/css/bootstrap.min.css',dirname(__FILE__)); ?>" rel="stylesheet">
<link href="<?php echo plugins_url('assets/css/dataTables.bootstrap.min.css',dirname(__FILE__)); ?>" rel="stylesheet">
<link href="<?php echo plugins_url('assets/css/bootstrap-editable.css',dirname(__FILE__)); ?>" rel="stylesheet">

<div style="padding: 15px;">
<div class="panel panel-primary">
	  <div class="panel-heading"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> <b>Builder Request for Review</b></div>
	  <div class="panel-body">
			<table id="builder-users" class="table table-striped table-bordered" cellspacing="0" width="100%">
		        <thead>
		            <tr>
		                <th>User Email</th>
		                <th>Subdivision</th>
		                <th>Plan</th>
		                <th>Unit</th>
		                <th>Elevation</th>
		                <th>Options</th>
		                <th>Is-Granted</th>
		                <th>Date Requested</th>
		            </tr>
		        </thead>
		 </table>
	 </div>
	</div>
</div>

<script src="<?php echo plugins_url('assets/js/jquery-1.11.3.min.js',dirname(__FILE__)); ?>"></script> 
<script src="<?php echo plugins_url('assets/js/jquery.dataTables.min.js',dirname(__FILE__)); ?>"></script>
<script src="<?php echo plugins_url('assets/js/dataTables.bootstrap.min.js',dirname(__FILE__)); ?>"></script>
<script src="<?php echo plugins_url('assets/js/bootstrap.min.js',dirname(__FILE__)); ?>"></script>
<script src="<?php echo plugins_url('assets/js/bootstrap-editable.min.js',dirname(__FILE__)); ?>"></script>
<script>
  $(document).ready(function() {
	  $.fn.editable.defaults.mode = 'popup';
	  
	  $('#builder-users').DataTable( 
	  		{
	        	"ajax": "<?php echo plugins_url('admin/trans/trans_builder_users.php',dirname(__FILE__)); ?>?funct=fetchforreview_data",
		        "columns": [
		            { "data": "email" },
		            { "data": "subdivision" },
		            { "data": "plan" },
		            { "data": "unit" },
		            { "data": "elevation" },
		            { "data": "options" },
		            { "data": "isgranted" },
		            { "data": "date_added" }
		        ]
		    }
	  );
	  
	  $('#builder-users').on( 'draw.dt', function () {
	    $('.status').editable({
		 	source: [
	              {value: 1, text: 'Active'},
	              {value: 0, text: 'InActive'}
	           ]   
	    });
	  });
  });
 
</script>