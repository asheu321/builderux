<?php
global $wpdb;

$rs = $wpdb->get_results("SELECT * FROM builder_lead_settings limit 1 ");

?>
<link href="<?php echo plugins_url('assets/css/bootstrap.min.css',dirname(__FILE__)); ?>" rel="stylesheet">

<div style="padding: 20px;">

	<div class="panel panel-primary">

	  <div class="panel-heading">Settings Management</div>
		  <div class="panel-body">
	
				<div class="alert alert-info" role="alert">LEADS: Please setup below value from salessimplicity api source and guid.</div>
				
				<div class="input-group">
				  <span class="input-group-addon" id="basic-addon1">Api Source/url</span>
				  <input type="text" id="apisrc" class="form-control" placeholder="url" aria-describedby="basic-addon1" value="<?php echo $rs[0]->api_source; ?>" >
				</div>
				<br />
				<div class="input-group">
				  <span class="input-group-addon" id="basic-addon1">GUID Value</span>
				  <input type="text" id="guidval" class="form-control" placeholder="guid value" aria-describedby="basic-addon1" value="<?php echo $rs[0]->guid; ?>" >
				</div>
				
				<br />
				<div class="alert alert-info" role="alert">FACEBOOK: Please setup below value from facebook api.</div>
				<div class="input-group">
				  <span class="input-group-addon" id="basic-addon1">Api ID</span>
				  <input type="text" id="apiid" class="form-control" placeholder="ID" aria-describedby="basic-addon1" value="<?php echo $rs[0]->fb_id; ?>" >
				</div>
				<br />
				<div class="input-group">
				  <span class="input-group-addon" id="basic-addon1">Api Key</span>
				  <input type="text" id="apikey" class="form-control" placeholder="Key" aria-describedby="basic-addon1" value="<?php echo $rs[0]->fb_key; ?>" >
				</div>
				<br />
				<div id="result" class="alert alert-success" role="alert">Settings successfuly save.</div>
				<p style="text-align: right;"><button class="btn btn-primary" type="button" onClick="saveLeads()">Save Settings</button></p>
		  </div>
	</div>
</div>
<script src="<?php echo plugins_url('assets/js/jquery-1.11.3.min.js',dirname(__FILE__)); ?>"></script> 
<script>
	$("#result").fadeOut();
	function saveLeads(){
		
		var apisrc = $('#apisrc').val();
		var guidval = $('#guidval').val();
		var apiid = $('#apiid').val();
		var apikey = $('#apikey').val();
		
		$.post( 
			"<?php echo plugins_url('admin/trans/trans_builder_master.php',dirname(__FILE__)); ?>?funct=update_lead", 
			{
				apisrc: apisrc, 
				guid: guidval,
				fbid: apiid,
				fbkey: apikey
			},
			function( data ) {
			  if(data.success){
				  $("#result").fadeIn();
				  $("#result").fadeOut(5000);
			  }
			},
			'json'
		);	
		
	}
</script>
