<?php

require_once get_home_path().'wp-load.php';
include get_home_path().'wp-content/plugins/builderux/xmlparser.php';
global $wpdb; 

if(@isset($_FILES['fileToUpload'])){
	
	$target_dir = str_replace('admin','uploads',__DIR__);
	$key = time();
	$target_file = $target_dir .'\/'.$key.'_'. basename($_FILES["fileToUpload"]["name"]);

	if(isset($_POST["submit"])) {
	    if(move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)){
		 	$wpdb->insert( 
				'builder_uploaded_xml', 
				array( 
					'filename' => $key.'_'. basename($_FILES["fileToUpload"]["name"]), 
					'date_uploaded' => @date('Y-m-d H:i:s') 
				) 
			); 
			
			
			$xml = simplexml_load_file($target_file) or die("Error: Cannot create object");  
	    }
	}
	
}

$result = $wpdb->get_results("select * from builder_uploaded_xml");
?>
<link href="<?php echo plugins_url('assets/css/bootstrap.min.css',dirname(__FILE__)); ?>" rel="stylesheet">
<div style="padding: 20px;">
	<div class="panel panel-primary">
	  <div class="panel-heading">Manual upload xml data source</div>
	  <div class="panel-body">
	  	<div class="alert alert-success" role="alert">
		    <form action="admin.php?page=admin_builder_uploadxml&action=uploadxml" method="post" enctype="multipart/form-data">
				<input type="hidden" name="MAX_FILE_SIZE" value="50000000" />
		    	<div class="row">
				  <div class="col-md-4"><input type="file" name="fileToUpload" id="fileToUpload"></div>
				  <div class="col-md-8"><input type="submit" value="Upload XML" name="submit"></div>
				</div>
			</form>
		</div>
		
		<table class="table table-striped">
			<tr><th>#</th><th>FileName</th><th>Date Uploaded</th></tr>
			<?php
				$cnt = 1;
				foreach($result as $key => $obj){ ?>
				<tr>
					<td><?php echo $cnt; ?></td>
					<td><?php echo $obj->filename; ?></td>
					<td><?php echo $obj->date_uploaded; ?></td>
				</tr>
				<?php $cnt++;
				}
			?>		
		</table>
	  </div>
	</div>
</div>