<?php

class PageTemplater {

        /**
         * A reference to an instance of this class.
         */
        private static $instance;

        /**
         * The array of templates that this plugin tracks.
         */
        protected $templates;

        /**
         * The array of styles.
         */
        protected $styles;


        /**
         * Returns an instance of this class. 
         */
        public static function get_instance() {

                if( null == self::$instance ) {
                        self::$instance = new PageTemplater();
                } 

                return self::$instance;

        } 

        /**
         * Initializes the plugin by setting filters and administration functions.
         */
        private function __construct() {

                $this->templates = array();


                // Add a filter to the attributes metabox to inject template into the cache.
                add_filter(
					'page_attributes_dropdown_pages_args',
					 array( $this, 'register_project_templates' ) 
				);


                // Add a filter to the save post to inject out template into the page cache
                add_filter(
					'wp_insert_post_data', 
					array( $this, 'register_project_templates' ) 
				);


                // Add a filter to the template include to determine if the page has our 
				// template assigned and return it's path
                add_filter(
					'template_include', 
					array( $this, 'view_project_template') 
				);


                // Add your templates to this array.
                $this->templates = array(
                        'templates/style1/style1.php'     => 'Builder Template 1',
                        'templates/style2/style2.php'     => 'Builder Template 2',
                        'templates/style3/style3.php'     => 'Builder Template 3',
                );

                // Add your styles to this array.
                $this->styles = array(
                        'templates/style1/inline-style1.php'     => 'Style 1',
                        'templates/style2/inline-style2.php'     => 'Style 2',
                        //'templates/style3/inline-style3.php'     => 'Style 3',
                );

                // WP-hook custom metabox
                add_action( 'add_meta_boxes', array( $this, 'builderux_add_meta_boxes' ) );
                add_action( 'save_post',      array( $this, 'save' ) );

                // Add frontend style
                add_action( 'wp_head', array( $this, 'add_frontend_style' ) );

                // Enqueue scripts
                add_action( 'wp_enqueue_scripts', array( $this, 'builderux_frontend_enqueue_scripts' ) );
				
        } 


        /**
         * Adds our template to the pages cache in order to trick WordPress
         * into thinking the template file exists where it doens't really exist.
         *
         */

        public function register_project_templates( $atts ) {

                // Create the key used for the themes cache
                $cache_key = 'page_templates-' . md5( get_theme_root() . '/' . get_stylesheet() );

                // Retrieve the cache list. 
				// If it doesn't exist, or it's empty prepare an array
				$templates = wp_get_theme()->get_page_templates();
                if ( empty( $templates ) ) {
                        $templates = array();
                } 

                // New cache, therefore remove the old one
                wp_cache_delete( $cache_key , 'themes');

                // Now add our template to the list of templates by merging our templates
                // with the existing templates array from the cache.
                $templates = array_merge( $templates, $this->templates );

                // Add the modified cache to allow WordPress to pick it up for listing
                // available templates
                wp_cache_add( $cache_key, $templates, 'themes', 1800 );

                return $atts;

        } 

        /**
         * Checks if the template is assigned to the page
         */
        public function view_project_template( $template ) {

                global $post;

                if (!isset($this->templates[get_post_meta( 
					$post->ID, '_wp_page_template', true 
				)] ) ) {
					
                        return $template;
						
                } 

                $file = plugin_dir_path(__FILE__). get_post_meta( 
					$post->ID, '_wp_page_template', true 
				);
				
                // Just to be safe, we check if the file exist first
                if( file_exists( $file ) ) {
                        return $file;
                } 
				else { echo $file; }

                return $template;

        }

        /**
         * Adds the meta box container.
         */
        public function builderux_add_meta_boxes( $post_type ) {
            // Limit meta box to certain post types.
            $post_types = array( 'page' );
     
            if ( in_array( $post_type, $post_types ) ) {
                add_meta_box(
                    'builderux_styles',
                    __( 'BuilderUX style', 'builderux' ),
                    array( $this, 'render_builderux_styles_content' ),
                    $post_type,
                    'side',
                    'high'
                );
            }
        }

        /**
         * Render BuilderUX Meta Box content.
         *
         * @param WP_Post $post The post object.
         */
        public function render_builderux_styles_content( $post ) {
     
            // Add an nonce field so we can check for it later.
            wp_nonce_field( 'builderux_custom_box', 'builderux_custom_box_nonce' );
     
            // Use get_post_meta to retrieve an existing value from the database.
            $value = get_post_meta( $post->ID, 'builderux_style', true );
     
            // Display the form, using the current value.
            ?>
            <label for="builderux_style">
                <?php _e( 'Page Style', 'builderux' ); ?>
            </label>
            <select id="builderux_style" name="builderux_style">
                <option value="">-</option>
                <?php
                if( $this->styles ) {
                    foreach( $this->styles as $key => $label ) {
                        if( $value == $key ) {
                            $selected = 'selected';
                        }else{
                            $selected = '';
                        }
                        echo '<option value="' . $key . '" ' . $selected . '>' . $label . '</option>';
                    }
                }
                ?>
            </select>
            <?php
        }

        /**
         * Save the meta when the post is saved.
         *
         * @param int $post_id The ID of the post being saved.
         */
        public function save( $post_id ) {
     
            // Check if our nonce is set.
            if ( ! isset( $_POST['builderux_custom_box_nonce'] ) ) {
                return $post_id;
            }
     
            $nonce = $_POST['builderux_custom_box_nonce'];
     
            // Verify that the nonce is valid.
            if ( ! wp_verify_nonce( $nonce, 'builderux_custom_box' ) ) {
                return $post_id;
            }
     
            /*
             * If this is an autosave, our form has not been submitted,
             * so we don't want to do anything.
             */
            if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
                return $post_id;
            }
     
            // Check the user's permissions.
            if ( 'page' == $_POST['post_type'] ) {
                if ( ! current_user_can( 'edit_page', $post_id ) ) {
                    return $post_id;
                }
            } else {
                if ( ! current_user_can( 'edit_post', $post_id ) ) {
                    return $post_id;
                }
            }
    
            $post_data = $_POST['builderux_style'];
     
            // Update the meta field.
            update_post_meta( $post_id, 'builderux_style', $post_data );
        }

        public function add_frontend_style() {
            global $post;

            $post_id = $post->ID;
            $get_style = get_post_meta( $post_id, 'builderux_style', true );
            if( $get_style ) {
                include plugin_dir_path( __FILE__ ) . $get_style;
            }
        }

        public function builderux_frontend_enqueue_scripts() {
            wp_enqueue_style( 'builderux-custom', plugins_url( 'assets/css/custom.css', __FILE__ ) );
            wp_enqueue_script( 'builderux-custom', plugins_url( 'assets/js/custom.js', __FILE__ ), array( 'jquery' ), 1.0, false );
        }

}

?>