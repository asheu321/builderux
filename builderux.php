<?php
/*
Plugin Name: Builder UX
Description: For realestate simplicity data api.
Version: 1
Author: John Clam
Author URI: 
*/

include 'builderpages.php';
include 'frontend/short_codes.php';
include 'buildertemplater.php';

register_activation_hook( __FILE__, 'install_builder_user');

register_activation_hook( __FILE__, 'install_choose_your_home_page');
register_activation_hook( __FILE__, 'install_where_we_build_page');
register_activation_hook( __FILE__, 'install_floor_plan_page');
register_activation_hook( __FILE__, 'install_model_house_page');
register_activation_hook( __FILE__, 'install_movein_ready_house_page');
register_activation_hook( __FILE__, 'install_floorplan_details_page');
register_activation_hook( __FILE__, 'install_moveinready_details_page');
register_activation_hook( __FILE__, 'install_model_details_page');
register_activation_hook( __FILE__, 'install_requestinfo_page');
register_activation_hook( __FILE__, 'install_subdivdetail_page');

function add_menu() {
	add_menu_page('BuilderUX', 'BuilderUX', 'manage_options', 'admin_builder_main','admin_builder_main');

	add_submenu_page( 'admin_builder_main', 'Builder Master', 'Builder Master', 'manage_options', 'admin_builder_master', 'admin_builder_master');
	add_submenu_page( 'admin_builder_main', 'Builder Division', 'Builder Division', 'manage_options', 'admin_builder_division', 'admin_builder_division');
	add_submenu_page( 'admin_builder_main', 'Subdivision', 'Subdivision', 'manage_options', 'admin_builder_subdivision', 'admin_builder_subdivision');
	add_submenu_page( 'admin_builder_main', 'Subdivision Plan', 'Subdivision Plan', 'manage_options', 'admin_builder_subdivisionplan', 'admin_builder_subdivisionplan');
	add_submenu_page( 'admin_builder_main', 'Builder Users', 'Builder Users', 'manage_options', 'admin_builder_users', 'admin_builder_users');
	add_submenu_page( 'admin_builder_main', 'Request for Review', 'Request for Review', 'manage_options', 'admin_builder_forreview', 'admin_builder_forreview');
	add_submenu_page( 'admin_builder_main', 'Slides', 'Slides', 'manage_options', 'admin_builder_front_settings', 'admin_builder_front_settings');	
	add_submenu_page( 'admin_builder_main', 'Upload XML', 'Upload XML', 'manage_options', 'admin_builder_uploadxml', 'admin_builder_uploadxml');	
	add_submenu_page( 'admin_builder_main', 'Other Settings', 'Other Settings', 'manage_options', 'admin_builder_leadsettings', 'admin_builder_leadsettings');
	
}

add_action('admin_menu', 'add_menu');
add_action( 'plugins_loaded', array( 'PageTemplater', 'get_instance' ) );

function admin_builder_main()
{
	include 'admin/admin_builder_main.php';
}

function admin_builder_master()
{
	include 'admin/admin_builder_master.php';
}

function admin_builder_division()
{
	include 'admin/admin_builder_division.php';
}

function admin_builder_subdivision()
{
	include 'admin/admin_builder_subdivision.php';
}

function admin_builder_subdivisionplan()
{
	include 'admin/admin_builder_subdivisionplan.php';
}

function admin_builder_users()
{
	include 'admin/admin_builder_users.php';
}

function admin_builder_forreview()
{
	include 'admin/admin_builder_forreview.php';
}

function admin_builder_front_settings()
{
	include 'frontend/front_builder_main.php';
}

function admin_builder_uploadxml()
{
	include 'admin/builder_uploadxml.php';
}

function admin_builder_leadsettings()
{
	include 'admin/builder_leadsettings.php';	
}

add_shortcode( 'get_slider', 'get_slider_func' );
add_shortcode( 'get_choose_home', 'get_choose_home_func' );
add_shortcode( 'get_where_we_build', 'get_where_we_build_func' );
add_shortcode( 'get_flooplan', 'get_floorplan_funct' );
add_shortcode( 'get_model_house', 'get_modelhouse_funct' );
add_shortcode( 'get_moveinready_house', 'get_move_in_ready_funct' );
add_shortcode( 'get_floorplan_details', 'get_floorplan_details_funct' );
add_shortcode( 'get_moveinready_details', 'get_moveinready_details_funct' );
add_shortcode( 'get_modelhouse_details', 'get_modelhouse_details_funct' );
add_shortcode( 'get_request_moreinfo', 'get_request_moreinfo_funct' );
add_shortcode( 'get_subdiv_details', 'get_subdiv_details_funct' );
add_shortcode( 'get_wherewebuild_map', 'get_wherewe_build_funct' );

?>