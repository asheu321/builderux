<?php

# ============= add builder user ===================

$user_id = 0;

function install_builder_user()
{
	global $user_id;
	
	$user = get_user_by( 'user_login', 'builderux' );
	
	if ( ! $user ) {

	    $userdata = array(
	          'user_login' => 'builderux',
	          'user_nicename' =>  'builderux' ,
	          'display_name' => 'builderux'
	    ); 
	     
	    $user_id = $newvalue = wp_insert_user( $userdata );
	    
    }else{
		
	    $user_id = $user->ID;
	    
		  $userdata = array(
		      'ID'            => $user->ID,
		      'user_login' 	  => 'builderux',
	          'user_nicename' =>  'builderux' ,
	          'display_name'  => 'builderux'
		  );
		
		  wp_update_user( $userdata );   
    }
}

# ============= pages installation =================

function install_choose_your_home_page()
{
	global $user_id;
	
	$page = get_page_by_title('Choose Your Home');
	
	if ( ! $page ) {

	    $post = array(
	          'comment_status' => 'closed',
	          'ping_status' =>  'closed' ,
	          'post_author' => $user_id,
	          'post_date' => date('Y-m-d H:i:s'),
	          'post_name' => 'builderux-choose-your-home',
	          'post_status' => 'publish' ,
	          'post_title' => 'Choose Your Home',
	          'post_content' => '[get_choose_home]',
	          'post_type' => 'page',
	    ); 
	     
	    $newvalue = wp_insert_post( $post, false );

	    update_option( 'builderux_choose_your_home', $newvalue );
	    
    }else{

		  $my_post = array(
		      'ID'           => $page->ID,
		      'post_content' => '[get_choose_home]',
		  );
		
		  wp_update_post( $my_post );   
    }
}

function install_where_we_build_page()
{
	global $user_id;
	
	$page = get_page_by_title('Where We Build');
	
	if ( ! $page ) {

	    $post = array(
	          'comment_status' => 'closed',
	          'ping_status' =>  'closed' ,
	          'post_author' => $user_id,
	          'post_date' => date('Y-m-d H:i:s'),
	          'post_name' => 'builderux-where-we-build',
	          'post_status' => 'publish' ,
	          'post_title' => 'Where We Build',
	          'post_content' => '[get_where_we_build]',
	          'post_type' => 'page',
	    ); 
	     
	    $newvalue = wp_insert_post( $post, false );

	    update_option( 'builderux-where-we-build', $newvalue );
	    
    }else{

		  $my_post = array(
		      'ID'           => $page->ID,
		      'post_content' => '[get_where_we_build]',
		  );
		
		  wp_update_post( $my_post );   
    }
}

function install_floor_plan_page()
{
	global $user_id;
	
	$page = get_page_by_title('Floor Plan');
	
	if ( ! $page ) {

	    $post = array(
	          'comment_status' => 'closed',
	          'ping_status' =>  'closed' ,
	          'post_author' => $user_id,
	          'post_date' => date('Y-m-d H:i:s'),
	          'post_name' => 'builderux-floor-plan',
	          'post_status' => 'publish' ,
	          'post_title' => 'Floor Plan',
	          'post_content' => '[get_flooplan]',
	          'post_type' => 'page',
	    ); 
	     
	    $newvalue = wp_insert_post( $post, false );

	    update_option( 'builderux-floor-plan', $newvalue );
	    
    }else{

		  $my_post = array(
		      'ID'           => $page->ID,
		      'post_content' => '[get_flooplan]',
		  );
		
		  wp_update_post( $my_post );   
    }
}

function install_model_house_page()
{
	global $user_id;
	
	$page = get_page_by_title('Model House');
	
	if ( ! $page ) {

	    $post = array(
	          'comment_status' => 'closed',
	          'ping_status' =>  'closed' ,
	          'post_author' => $user_id,
	          'post_date' => date('Y-m-d H:i:s'),
	          'post_name' => 'builderux-model-house',
	          'post_status' => 'publish' ,
	          'post_title' => 'Model House',
	          'post_content' => '[get_model_house]',
	          'post_type' => 'page',
	    ); 
	     
	    $newvalue = wp_insert_post( $post, false );

	    update_option( 'builderux-model-house', $newvalue );
	    
    }else{

		  $my_post = array(
		      'ID'           => $page->ID,
		      'post_content' => '[get_model_house]',
		  );
		
		  wp_update_post( $my_post );   
    }
}


function install_movein_ready_house_page()
{
	global $user_id;
	
	$page = get_page_by_title('Move In Ready House');
	
	if ( ! $page ) {

	    $post = array(
	          'comment_status' => 'closed',
	          'ping_status' =>  'closed' ,
	          'post_author' => $user_id,
	          'post_date' => date('Y-m-d H:i:s'),
	          'post_name' => 'builderux-movein-ready-house',
	          'post_status' => 'publish' ,
	          'post_title' => 'Move In Ready House',
	          'post_content' => '[get_moveinready_house]',
	          'post_type' => 'page',
	    ); 
	     
	    $newvalue = wp_insert_post( $post, false );

	    update_option( 'builderux-movein-ready-house', $newvalue );
	    
    }else{

		  $my_post = array(
		      'ID'           => $page->ID,
		      'post_content' => '[get_moveinready_house]',
		  );
		
		  wp_update_post( $my_post );   
    }
}

function install_floorplan_details_page()
{
	global $user_id;
	
	$page = get_page_by_title('Floor Plan Details');
	
	if ( ! $page ) {

	    $post = array(
	          'comment_status' => 'closed',
	          'ping_status' =>  'closed' ,
	          'post_author' => $user_id,
	          'post_date' => date('Y-m-d H:i:s'),
	          'post_name' => 'builderux-floor-plan-details',
	          'post_status' => 'publish' ,
	          'post_title' => 'Floor Plan Details',
	          'post_content' => '[get_floorplan_details]',
	          'post_type' => 'page',
	    ); 
	     
	    $newvalue = wp_insert_post( $post, false );

	    update_option( 'builderux-floor-plan-details', $newvalue );
	    
    }else{

		  $my_post = array(
		      'ID'           => $page->ID,
		      'post_content' => '[get_floorplan_details]',
		  );
		
		  wp_update_post( $my_post );   
    }
}

function install_moveinready_details_page()
{
	global $user_id;
	
	$page = get_page_by_title('Move In Ready Details');
	
	if ( ! $page ) {

	    $post = array(
	          'comment_status' => 'closed',
	          'ping_status' =>  'closed' ,
	          'post_author' => $user_id,
	          'post_date' => date('Y-m-d H:i:s'),
	          'post_name' => 'builderux-movein-ready-details',
	          'post_status' => 'publish' ,
	          'post_title' => 'Move In Ready Details',
	          'post_content' => '[get_moveinready_details]',
	          'post_type' => 'page',
	    ); 
	     
	    $newvalue = wp_insert_post( $post, false );

	    update_option( 'builderux-movein-ready-details', $newvalue );
	    
    }else{

		  $my_post = array(
		      'ID'           => $page->ID,
		      'post_content' => '[get_moveinready_details]',
		  );
		
		  wp_update_post( $my_post );   
    }
}

function install_model_details_page()
{
	global $user_id;
	
	$page = get_page_by_title('Model House Details');
	
	if ( ! $page ) {

	    $post = array(
	          'comment_status' => 'closed',
	          'ping_status' =>  'closed' ,
	          'post_author' => $user_id,
	          'post_date' => date('Y-m-d H:i:s'),
	          'post_name' => 'builderux-model-house-details',
	          'post_status' => 'publish' ,
	          'post_title' => 'Model House Details',
	          'post_content' => '[get_modelhouse_details]',
	          'post_type' => 'page',
	    ); 
	     
	    $newvalue = wp_insert_post( $post, false );

	    update_option( 'builderux-model-house-details', $newvalue );
	    
    }else{

		  $my_post = array(
		      'ID'           => $page->ID,
		      'post_content' => '[get_modelhouse_details]',
		  );
		
		  wp_update_post( $my_post );   
    }
}

function install_requestinfo_page()
{
	global $user_id;
	
	$page = get_page_by_title('Request More Info');
	
	if ( ! $page ) {

	    $post = array(
	          'comment_status' => 'closed',
	          'ping_status' =>  'closed' ,
	          'post_author' => $user_id,
	          'post_date' => date('Y-m-d H:i:s'),
	          'post_name' => 'builderux-more-info',
	          'post_status' => 'publish' ,
	          'post_title' => 'Request More Info',
	          'post_content' => '[get_request_moreinfo]',
	          'post_type' => 'page',
	    ); 
	     
	    $newvalue = wp_insert_post( $post, false );

	    update_option( 'builderux-more-info', $newvalue );
	    
    }else{

		  $my_post = array(
		      'ID'           => $page->ID,
		      'post_content' => '[get_request_moreinfo]',
		  );
		
		  wp_update_post( $my_post );   
    }
}

function install_subdivdetail_page()
{
	global $user_id;
	
	$page = get_page_by_title('Subdivision Details');
	
	if ( ! $page ) {

	    $post = array(
	          'comment_status' => 'closed',
	          'ping_status' =>  'closed' ,
	          'post_author' => $user_id,
	          'post_date' => date('Y-m-d H:i:s'),
	          'post_name' => 'builderux-subdiv-details',
	          'post_status' => 'publish' ,
	          'post_title' => 'Subdivision Details',
	          'post_content' => '[get_subdiv_details]',
	          'post_type' => 'page',
	    ); 
	     
	    $newvalue = wp_insert_post( $post, false );

	    update_option( 'builderux-subdiv-details', $newvalue );
	    
    }else{

		  $my_post = array(
		      'ID'           => $page->ID,
		      'post_content' => '[get_subdiv_details]',
		  );
		
		  wp_update_post( $my_post );   
    }
}

#===================================================


?>