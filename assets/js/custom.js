jQuery(document).ready(function($) {
	jQuery('body').on('click', '.grp', function(e) {
		var idGrp = jQuery(this).attr('id');
		jQuery('.cld:not(.item-selected)').fadeOut();
		jQuery('.'+idGrp).fadeIn();
	});

	jQuery('body').on('click', 'input[type="checkbox"]', function(e) {
		var check = jQuery(this).prop('checked');
		if(check === true) {
			jQuery(this).closest('.cld').addClass('item-selected');
		}else{
			var closestElm = jQuery(this).closest('.cld');
			var findCheckbox = closestElm.find('.lt_td3').find('.lt_div').find('.lt_desc_input').find('input[type="checkbox"]:checked');
			if( findCheckbox.length === 0 ) {
				closestElm.removeClass('item-selected');
			}
		}
	});
});