<style type="text/css">
body .wwb_table {
    margin-top: 40px;
}

.wwb_table .wwb_table_td {
    display: block;
    padding-bottom: 0;
}

body .wwb_table tr .wwb_table_td .wwb_div {
    display: table !important;
    margin-bottom: 40px !important;
    margin-left: 0;
    margin-right: 0;
    width: 100% !important;
}

body .wwb_table tr .wwb_table_td .wwb_img {
    float: right !important;
    height: 100% !important;
    width: 54% !important;
}

.wwb_table .wwb_table_td .wwb_h4 {
    margin-right: 56%;
}

.wwb_table .wwb_table_td .wwb_div > p {
    margin-right: 56%;
}
</style>
