<style>

	body {
	 margin: 0px;
	 padding: 0px;
	 font-family: myriad pro;
	}
	
	* {
	 text-decoration: none;
	}
	
	.stye1-banner {
	  background: url('images/bg-banner.jpg') no-repeat top center; 
	  background-size: cover;
	  padding:40px 0px;
	}
	
	.s1-container {
	  width: 85%;
	  margin: auto;
	  overflow: hidden;
	}
	
	.s1-logo {
	 float: left;
	}
	
	#s1-menu {
	 float: right;
	 margin: 15px 0px;
	}
	
	#s1-menu ul {
	 margin: 0px;
	 padding: 0px;
	}
	
	#s1-menu ul li {
	 display: block;
	 float: left;
	}
	#s1-menu ul li a {
	 padding: 10px;
	 color: #fff;
	 font-weight: bold;
	}
	#s1-menu ul li a:hover {
	 color: #6fa91a;
	}
	
	.s1-build-btn {
	 text-align: center;
	 padding: 200px 0px;
	}
	
	#s1-content {
	 background: #34495e;
	 padding: 60px 0px;
	}
	
	.s1-light {
	 color: #fff;
	}
	
	.s1-center {
	 text-align: center;
	}
	.s1-light p {
	 line-height: 25px;
	}
	
	#s1-footer {
	 padding: 50px 0px;
	 background: #292f32;
	 padding-bottom: 30px;
	}
		
		
	.s1-footer-widget {
	 width: 30%;
	 float: left;
	 margin-right: 35px;
	}
	
	.s1-footer-widget h2 {
	 margin-top: 0px;
	}
	
	.s1-footer-widget ul li {
	 list-style: none;
	 padding: 4px 0px;
	}
	
	.s1-footer-widget ul {
	 margin: 0px;
	 padding: 0px;
	}
	
	.s1-footer-widget ul li img {
	 margin-right: 10px  ; 
	 vertical-align: middle;
	 
	}
	
	.social-media img {
	 margin-right: 7px;
	}
	
	#copyright {
	 text-align: center;
	 clear: both;
	 color: #e8e8e8;
	 margin-top: 50px;
	 font-size: 11px;
	}
	
</style>