<?php get_header(); ?>

<style>

body {
	background-color:#fff;
}

*/


/*PLUGIN CSS */

#scontent {
	display: none;
	   
}

.slider_first_div {
border: 0px!important;
}

.slider_btn {
	border: 0px;
	background: url('wp-content/plugins/builderux//assets/images/call-to-cation-btn.png') no-repeat center !important;
	width: 254px;
	height: 76px;
	
}

.slider_third_div { 
bottom: 104px!important;

}

#step1, #step2, #step3, #step4, #step5, #step6 {
    background: #115ea2;
    color: #fff;
    font-weight: bold;
	border-right: 1px solid #4B83B5;
}


.wwb_div {
-webkit-box-shadow: 7px 7px 5px -4px rgba(230,230,230,1);
-moz-box-shadow: 7px 7px 5px -4px rgba(230,230,230,1);
box-shadow: 7px 7px 5px -4px rgba(230,230,230,1);
background: #ffffff;
margin: 0px 10px;
padding: 0px!important;
    width: 23%!important;
	display: table-cell;
	float: none!important;

}


.wwb_h4 {
 background: #115EA2;
    padding: 5px;
	color: #FFFFFF!important;
}

.wwb_div p {
 font-size: 12px;
}

.master-holder strong {
 color: #115ea2!important;
}

.wwb_ul li {
    border-bottom: 1px solid rgba(194, 194, 194, 0.27);
}

li.divider {
 display: none;
}

.wwb_ul {
 margin: 0px!important;
 background: none!important;

}

.wwb_ul li a {
 font-size: 12px;
 color: #115ea2!important;
}

.wwb_mkdescription {
 padding: 16px;
}

.tbl-floorplan tr td {
 text-align: left;
}

#theme-page select {
 min-width: 100%!important;
}

.wwb_table_td {

text-align: center;
    padding: 12px;
    display: table;
    border-collapse: separate;
    border-spacing: 18px;
}

.flr_row {
 width: 100%;
 clear: both;
     overflow: hidden;
    padding: 11px;
    border-bottom: 1px solid #f2f2f2;
}

.d-left {
 width: 80%;
 font-weight: bold;
 color: #7BA7C5;
}

.d-right {
 width: 20%;
 text-align: right;
}

.flr_details_row table  {
 margin-bottom: 20px!important;
}

.flr_details_row {
 width: 70%;
}

.tbl-floorplan tr td {
 vertical-align: top;
}

.td-flr_details {
 width: 48%;
}

.flr_row:nth-of-type(odd) {
 background: #fafafa;
}

.flr_row ul li {
 list-style: none!important;
 margin: 0px;
 float: left;
}

.flr_row ul {
 margin: 0px;
}

.tbl-floorplan tr {
 margin-bottom: 40px;
 display: block;
}

.flr_details_row table tr {
 margin: 0px!important;
}


.fp_view {

  font-family: Arial;
  color: #ffffff;
  font-size: 14px;
  background: #ff551a;
  padding: 10px 13px 10px 13px;
  text-decoration: none;
  margin-top: 10px;
}

.fp_view:hover {
  background: #3cb0fd;
  background-image: -webkit-linear-gradient(top, #3cb0fd, #3498db);
  background-image: -moz-linear-gradient(top, #3cb0fd, #3498db);
  background-image: -ms-linear-gradient(top, #3cb0fd, #3498db);
  background-image: -o-linear-gradient(top, #3cb0fd, #3498db);
  background-image: linear-gradient(to bottom, #3cb0fd, #3498db);
  text-decoration: none;
  color: #ffffff!important;
}


.mvin_view {
  -webkit-border-radius: 7;
  -moz-border-radius: 7;
  border-radius: 7px;
  text-shadow: 0px 1px 0px #a14f40;
  font-family: Arial;
  color: #ffffff;
  font-size: 14px;
  background: #ff551a;
  padding: 10px 13px 10px 13px;
  text-decoration: none;
  margin-top: 10px;
}


.mvin_view:hover {
  background: #3cb0fd;
  background-image: -webkit-linear-gradient(top, #3cb0fd, #3498db);
  background-image: -moz-linear-gradient(top, #3cb0fd, #3498db);
  background-image: -ms-linear-gradient(top, #3cb0fd, #3498db);
  background-image: -o-linear-gradient(top, #3cb0fd, #3498db);
  background-image: linear-gradient(to bottom, #3cb0fd, #3498db);
  text-decoration: none;
  color: #ffffff!important;
}

.mvin_table tr td {
 vertical-align: top;
 text-align: left;
}




.mvin_second_td span {
 display: block;
}

.mvin_h4 {
    font-size: 24px!important;
    color: #115ea2!important;
}

.mvin_second_td span:nth-child(1) {
   color: #5ac763;
    font-weight: 600;
}

.mvin_second_td span:nth-child(2) {
  font-size: 26px;
    font-weight: 600;
}

.mvin_second_table tr {
 margin: 0px!important;
}

.mvin_tr {
 margin-bottom: 50px;
display: block;
}
 
 
 .mvin_second_table tr td {
  border-right: 1px solid #f1f1f1;
 }
 
  .mvin_second_table tr td:last-child {
  border-right: 0px;
 }
 
 .mvin_table_td2 p strong {
  font-size: 16px;
 }
 
 .rq_td label {
     display: block;
    color: #fff;
    font-size: 20px;
    font-weight: bold;
    text-transform: uppercase;
    padding-bottom: 11px;
 }
  .rq_td {
  background: #2aacff!important;
  }

 
#fname, #lname, #phone, #email, #traffic_source, #traffic_source , #occupation_source, #family_size_source, #message{

 background: transparent;
    border-bottom: 1px solid hsla(0, 0%, 100%, 0.48)!important;
    color: #fff!important;
    width: 47%;
    text-align: center;
	border-left: 0px!important;
    border-top: 0px!important;
    border-right: 0px!important;
}

::-webkit-input-placeholder {
   color: #fff;
}

:-moz-placeholder { /* Firefox 18- */
   color: #fff;  
}

::-moz-placeholder {  /* Firefox 19+ */
   color: #fff;  
}

:-ms-input-placeholder {  
   color: #fff;  
}

#message {
 width: 47%;
 text-align: left!important;
}

#rsend {
 border: 0px;
 background: #fff!important;
 color: #2aacff;
 font-weight: bold;
 padding: 10px 20px;
}

select option {
color: #000!important;
}

.mhd_specs {
 background: #2aacff!important;
 color: #fff!important;
 padding: 12px;
}

.mhd_dt, .mhd_opt {
 color: #fff!important;
 font-size: 27px;
}
.mhd_tbl td {
 padding: 7px!important;
 text-align: left!important;
 border-bottom: 1px solid hsla(0, 0%, 100%, 0.17)!important;

}

.mhd_tbl tr:nth-child(1) td {
 font-weight: bold;
 font-size: 20px;
}

.mhd_h4 {
 font-size: 27px!important;
    color: #2AACFF!important;
	margin-bottom: 0px!important;
}

.mhd_td p {
 text-align: left;
}

#restriction {
 margin-top: 0px!important;
}

.mhd_dvimg {
   -webkit-box-shadow: 8px 8px 0px -4px rgba(201,199,201,1);
-moz-box-shadow: 8px 8px 0px -4px rgba(201,199,201,1);
box-shadow: 8px 8px 0px -4px rgba(201,199,201,1);
	border: 1px solid #ccc!important;
	margin-right: 18px!important;
	margin-bottom: 18px!important;
}

#lot-images, #fplan-images, #mplan-images {
 overflow: hidden!important;
}

.pln_heading {
 margin-top: 46px!important;
 font-size: 23px;
}

#lot-option table tr:nth-of-type(odd) td {
background: #6CC3FB;
}

#lot-option table tr td {
 text-align: left;
 padding: 7px!important;
    border-bottom: 1px solid hsla(0, 0%, 100%, 0.17)!important;
}

.no-nth tr:nth-child(1) td {
    font-weight: normal;
    font-size: 15px;
}

#step-html table {
    display: table;
    border-collapse: separate;
    border-spacing: 18px;
}

#step-html table tr td div {

webkit-box-shadow: 7px 7px 5px -4px rgba(230,230,230,1);
    -moz-box-shadow: 7px 7px 5px -4px rgba(230,230,230,1);
    box-shadow: 7px 7px 5px -4px rgba(230,230,230,1);
    background: #ffffff;
    margin: 0px 10px;
    padding: 0px;
    width: 23%;
    display: table-cell;
    float: none;

}
#step-html table tr td div  h3 {

    background: #7BA7C5;
    padding: 5px;
    color: #FFFFFF!important;
	font-size: 18px;

}

#step-html table tr td div p {
 padding: 0px 8px;
 font-size: 13px!important;
}


#step-html table tr td div input[type=button] {
	background: #2aacff;
	padding: 8px 18px;
	border: 0px;

color: #fff;
font-weight: bold;
}

#step-html table tr td div input[type=button]:hover {

	background: #1fa1f2!important;
}

#bux-div {
    width: 54%!important;
	left: 16%;
}

#bux-overlay {
 background-color: rgba(34, 34, 34, 0.48)!important;
}

#user_reg_form input {
 width: 100%!important;
}

#user_reg_form input[type=password] {
 width: 91%!important;
}

#user_reg_form input[type=button] {
background: #2aacff;
    padding: 8px 18px;
    border: 0px;
    color: #fff;
    font-weight: bold;
}

.cur_customer  input {
 width: 100%!important;
}

.cur_customer input[type=password] {
 width: 91%!important;
}

.cur_customer input[type=button] {
background: #2aacff;
    padding: 8px 18px;
    border: 0px;
    color: #fff;
    font-weight: bold;
}

#map-canvas {
 background: none!important;
 text-align: center;
}

#loginstatus a {
-webkit-border-radius: 5px;
-moz-border-radius: 5px;
border-radius: 5px;
color: #fff!important;
font-size: 12px;
padding: 6px 7px;
    background: #ff551a;
}

#loginstatus {
    margin-top: 6px;
}

.topo_map_table {
    float: right;
    width: 27%;
	background: #2aacff;
}

.topo_map_table td a {
 text-transform: uppercase;
}

.topo_map_table td.topo_map_table {
	display: block;
}

.topo_map_table td {
    display: -webkit-box;
    width: 92%!important;
    clear: both;
	 border-bottom: 1px solid hsla(0, 0%, 100%, 0.17)!important;
}

#topo-mapper {
    padding: 0px!important;
    float: left;
    width: 71%;
}

#topo-mapper img {
 width: 100%;
}
.topo_map_table td a {
    color: #fff;
   
    display: block;
    width: 100%;
    font-weight: bold;
}

.mhd_dvimg {
 width: 46%;
}


.wwb_td_d_1{
 padding-top: 0px;
}

.wwb_left_d {
 padding-top: 0px;
}

.wwb_left_d h4 {
  color: #fff!important;    
  font-size: 27px!important;
  font-weight: bold!important;
  
}

.wwb_left_d ul {
 margin: 0px;
}

.wwb_left_d ul li {
 margin: 0px;
     border-bottom: 1px solid hsla(0, 0%, 100%, 0.17)!important;
    padding: 5px 0px;
}

.wwb_left_d ul li a {
 color: #fff!important;
}

.wwb_mapview {
 margin-top: 30px;
}

.wwb_mapview h4 {
 color: #000!important;
}

.wwb_details_m td {
 padding: 0px!important;
}

.wwb_mapview #map-canvas {
 height: 450px!important;
}

.page-id-90  #mk-page-id-90 {
 margin: 0px!important;
 max-width: 100%;
}

.page-id-90 .theme-content  {
 padding: 0px!important;
}

.wwb_img_main img {
 width: 100%!important;
}

.wwb_img_main {
 height: 500px;
 overflow: hidden;
}

.wwb_sec {
 padding: 19px 0px;
 width: 87%;
 margin: auto;
}


.wwb_sec .mvin_second_table tr td {
 width: 200px;
}

.wwb_lists {
 margin: 25px 0px;
}

.wwb_lists li a {
 padding: 10px 14px;
 background: #115ea2;
 color: #fff;
}

.wwb_lists li a:hover {
background-color: rgb(42, 172, 255);
}

.wwb_lists li {
 margin: 0px;
 display: inline;
}

.wwb_sec  .mvin_second_td span:nth-child(2) {
 font-size: 18px!important;
}

.tbl_details_wwb {
    margin-top: 53px!important;
}

.tbl_details_wwb td {
 text-align: left;
 padding: 12px!important;
}

.tbl_details_wwb h4 {
    color: #115ea2!important;
}

.tbl_details_wwb_left {
 width: 45%;
}

.tbl_details_wwb #map-canvas {
 height: 620px!important;
}


.tbl_details_wwb_left object {
 width: 100%!important;
}

.tbl_details_wwb_right {
 width: 400px!important;
}

.cs_td .cs_img {
 width: 100%!important;
 height: auto!important;
}

.cs_table td {
 vertical-align: top!important;
 text-align: left;
 padding: 0px!important;
}

.cs_td {
 width: 420px!important;
 
}

.cs_td div {
    display: initial!important;
}

.tbl_data {
border-collapse: collapse!important;
}

.cs_table2 {
border-collapse: collapse!important;
}

.data_btn input {
 border: 0px!important;
}

.gm-style-iw img {
 margin-top: 27px;
}

.map_desc {
 text-align: left;
}

.btn-primary {
background: #ff551a;
border: 0px;
color: #fff;
padding: 5px 8px;
}

.lt_div img {
height: 250px!important;
    width: 93%!important;
    padding: 4%!important;

}



.lt_desc_input {
    padding-top: 12px!important;
    border-top: solid 1px #eaebed!important;
    background: #f1f6fa!important;
    margin-bottom: 0px!important;
    -webkit-border-bottom-right-radius: 4px;
    -webkit-border-bottom-left-radius: 4px;
    -moz-border-radius-bottomright: 4px;
    -moz-border-radius-bottomleft: 4px;
    border-bottom-right-radius: 4px;
    border-bottom-left-radius: 4px;
	padding-bottom: 12px!important;
	    color: #666b6f;
    font-weight: bold;
}

.lt_div {
    border: solid 0px #dee2eb!important;
	    padding: 0px!important;
	-webkit-box-shadow: 0px 0px 10px 2px rgba(217,222,226,1)!important;
-moz-box-shadow: 0px 0px 10px 2px rgba(217,222,226,1)!important;
box-shadow: 0px 0px 10px 2px rgba(217,222,226,1)!important;
-webkit-border-radius: 4px;
-moz-border-radius: 4px;
border-radius: 4px;
}
.lt_table2 {
 border-spacing: 12px;
}

.lt_td1 {
 padding: 0px!important;
}

.lt_td6 {
    float: right!important;
    width: 22%!important;
   
    margin-top: 59px;
}

.lt_td7 {
 background: #2aacff!important;
    color: #fff;
    padding: 10px;
}




.lt_table3 {
margin: 0px;
}

.lt_table3 li {
border-bottom: 1px solid hsla(0, 0%, 100%, 0.17)!important;
    list-style: none;
	    padding: 6px 0px;
		font-weight: bold;
}

.lt_table {
    border-collapse: collapse!important;
	    width: 64%;
    float: left;
}

.lt_table2 {
border-spacing: 14px 0px!important;
border-collapse: separate!important;
}

.page-id-81  #theme-page {
 background: #eef3f7!important;
}

.lt_desc_input select {
 margin-top: 10px;
 background: #ffffff!important;
}

.lt_td3 {
 padding-left: 0px!important;
 padding-right: 0px!important;
}

.lt_td2 {
 background: transparent!important;
 font-size: 30px!important;
 color
}

#optcalc tr td {
 padding: 6px;
}



#optcalc tr td:nth-child(2) {
 text-align: right;
}

#optcalc tr td:nth-child(1) {
 font-weight: bold;
     text-align: left;
}

.lt_td6 p strong {
color: #fff!important;
    font-size: 21px;

}

#info-total {
    margin-top: 30px;
    font-size: 16px;
    text-align: center;
}

#info-total strong {
 display: block;
 font-size: 21px!important;
 color: #fff!important;
}

#optcalc {
 border-collapse: collapse!important;
}

input.fp_view_btn {
 border: 0px!important;
     width: 100%;
    margin: auto;
	color: #ffffff;
    font-size: 14px;
    background: #ff551a;
    padding: 10px 13px 10px 13px;
}

input.fp_view_btn:hover {
 background: #fff!important;    
 color: #000!important;
 
}

.lt_td6 p {
	text-align: center;
    padding-top: 35px;
}


#optcalc tr:nth-of-type(odd) {
background: #68c2fc!important;
padding: 3px!important;
}

#optcalc tr:nth-of-type(even) {
 color: #115EAA!important;
} 

.lt_desc {
margin-top: 14px;
    font-weight: bold;
    color: #115ea2;
    text-align: left;
	height: 42px;
	    text-transform: uppercase;
    line-height: 15px;
	padding: 0px 11px!important;
}

.lt_desc_input input {
 background: #ffffff!important;
}

.tbl_elevation .elev_div {
 float: none!important;
}



.lt_table2 .lt_div {
 width: 31%!important;
}




/*RESPONSIVE */


@media handheld, only screen and (max-width: 720px) {

.wwb_table .wwb_div {
width: 100%!important;
    display: BLOCK!IMPORTANT;
	margin: 0px!important;
}

.wwb_table_td {
border-collapse: collapse;

}

.wwb_table .wwb_div .wwb_img {
 height: auto!important;
}

.wwb_table .wwb_div input[type=button] {
margin-bottom: 17px;

}
.tbl_elevation .elev_div {
 display: block!important;
 width: 100%!important;
 margin-left: 0px!important;
 margin-bottom: 15px!important;
}


}


@media handheld, only screen and (max-width: 600px) {
.topo_map_table {
 float: none;
 width: 100%;
}

#topo-mapper  {
 float: none;
 width: 100%;
}


}

@media handheld, only screen and (max-width: 850px) {
#optcalc tr td {
	display: block;
    text-align: center!important;
}
}



@media handheld, only screen and (max-width: 1132px) {
.lt_table2  .lt_div {
 width: 30%!important;
}

.lt_table {
    width: 74%!important;
}

.lt_td1 {
 width: 70%;
}

.lt_table2 {
    border-collapse: collapse!important;
}

.lt_div {
 float: left!important;
}
}

@media handheld, only screen and (max-width: 930px) {
.lt_table2 .lt_div {
    width: 47%!important;
	margin-bottom: 18px!important;
}
}


@media handheld, only screen and (max-width: 730px) {
.lt_table2 .lt_div {
    width: 97%!important;
	margin-bottom: 18px!important;
}
.lt_table {
    width: 65%!important;
}

.lt_td6 {
width: 32%!important;
}
}

@media handheld, only screen and (max-width: 530px) {
.lt_table {
    width: 100%!important;
	float: none;
}

.lt_td6 {
width: 100%!important;
float: none;
}

#optcalc tr td {
 display: table-cell;

 padding: 10px!important;
}

#optcalc tr td:nth-child(2) {
 text-align: right!important;
}

#optcalc tr td:nth-child(1) {
 text-align: left!important;
}

.flr_details_row table tr .td-flr_details {
display: block;
    width: 100%;
}

.td-flr_descr {
 padding: 0px!important;
}

}


@media handheld, only screen and (max-width: 680px) {
.wwb_lists li {
width: 48%;
    float: left;
    margin: 4px;
}

.wwb_lists li a {
    width: 92%;
    display: block;
}

.wwb_details_m tbody {
 display: block;
}

.wwb_details_m {
 display: block;
}

.wwb_details_m tbody tr {
 display: block;
}

.tbl_details_wwb {
 display: block;
}

.tbl_details_wwb tbody {
 display: block;
}

.tbl_details_wwb tbody tr {
 display: block;
}

.tbl-floorplan .tbl_flr_img {
 display: block;
 width: 96%!important;
}

.tbl-floorplan .tbl_flr_img  img {
 width: 100%!important;
}

}

@media handheld, only screen and (max-width: 820px)  {
.tbl_details_wwb {
 width: 100%!important;
}
}

@media handheld, only screen and (max-width: 520px) {
.tbl_details_wwb td {
 display: block;
 width: 99%;
}

.wwb_lists li {
 width: 100%;
}

.tbl_details_wwb td {
 padding: 12px 0px!important;
}

.wwb_details_m tr:nth-child(2) td {
    margin: auto;
    content: "";
    display: block;
}

.wwb_details_m tr:nth-child(2) td .wwb_img_main {
    width: 100%;
    height: 100%;
}


}

@media handheld, only screen and (max-width: 720px) {
.mhd_dvimg {
 width: 100%;
}

.mhd_td {
 padding-right: 15px!important;
}

.mhd_dvimg img {
 width: 100%!important;
}

.mhd_table td {
 padding: 0px;
}

.mhd_tddetail {
 width: 100%!important;
}

.mvin_table_td {
 width: 96%!important;
 display: block;
}

.mvin_table_td img {
 width: 100%!important;
 height: auto!important;
}

.mvin_table {
 display: block
}

.mvin_table tbody {
 display: block
}

.mvin_table tbody  tr{
 display: block
}

.mvin_table tbody  tr td {
 display: block;
}

.mvin_second_table tr td {
 padding: 12px;
 display: inline-block!important;
}



}



@media handheld, only screen and (max-width: 740px) {
.wwb_img_main {
 height: auto!important;
}
}

@media handheld, only screen and (max-width: 560px) {
	.mhd_table .mhd_td {
	 display: block;
	 width: 100%!important;
	}
	
	.mhd_tddetail {
	 display: block;
	}
	
	.mhd_td #images {
		 overflow: hidden;
	}
}

@media handheld, only screen and (max-width: 500px) {
	.mvin_second_table td {
		width: 42%!important;
		padding: 0px!important;
		padding-bottom: 12px!important;
	}
}

.slider_fourth_div {
	visibility: hidden;	
}

.fullwidth {
	width: 100%;	
}

.wwb_div_caption {
	margin-top: 50px;	
}

.wwb_h1_title {
	text-align: center;	
}
	
</style>

<div id="primary" class="content-area fullwidth">
	<main id="main" class="site-main" role="main">
		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

			// Include the page content template.
			get_template_part( 'template-parts/content', 'full' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) {
				comments_template();
			}

			// End of the loop.
		endwhile;
		?>

	</main><!-- .site-main -->

	<?php get_sidebar( 'content-bottom' ); ?>

</div><!-- .content-area -->

<?php get_footer(); ?>