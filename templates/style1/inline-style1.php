<style type="text/css">
.wwb_table .wwb_table_td {
    display: block;
    padding-bottom: 0;
}

body .wwb_table tr .wwb_table_td .wwb_div {
    display: table !important;
    margin-bottom: 40px !important;
    width: 100% !important;
}

body .wwb_table tr .wwb_table_td .wwb_img {
    float: left !important;
    height: 100% !important;
    width: 54% !important;
}

.wwb_table .wwb_table_td .wwb_h4 {
    margin-left: 56%;
}

.wwb_table .wwb_table_td .wwb_div > p {
    margin-left: 56%;
}
</style>
